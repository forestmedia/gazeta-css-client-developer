var AWS = require('aws-sdk');

AWS.config.update({
  region: "us-east-1"
});

var docClient = new AWS.DynamoDB();

//Check if Table exists
docClient.describeTable({TableName: 'Hashtag'}, function(err) {
    if (!err) console.log("Hashtag Table Exists"); // an error occurred

    else{

      //Create Hashtag Table
      var params = {
          TableName: 'Hashtag',
          KeySchema: [
              { AttributeName: 'label', KeyType: 'HASH',
              },
              {
                  AttributeName: '_id', KeyType: 'RANGE',
              }
          ],
          AttributeDefinitions: [
              {
                  AttributeName: 'label',
                  AttributeType: 'S',
              },
              {
                  AttributeName: '_id',
                  AttributeType: 'S',
              },
              {
                  AttributeName: 'published_date',
                  AttributeType: 'N',
              }

          ],
          ProvisionedThroughput: {
              ReadCapacityUnits: 5,
              WriteCapacityUnits: 1,
          },

          LocalSecondaryIndexes: [
              {
                  IndexName: 'date-index',
                  KeySchema: [
                      {
                          AttributeName: 'label', KeyType: 'HASH'
                      },
                      {
                          AttributeName: 'published_date', KeyType: 'RANGE'
                      }
                  ],
                  Projection: {
                      ProjectionType: 'ALL'
                  }
              }
          ]
      };

      docClient.createTable(params, function(err, data) {
          if (err) console.error(err); // an error occurred
          else console.log(data); // successful response

      });

    }
})
