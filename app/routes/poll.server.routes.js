/*'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller.dynamo');
	var polls = require('../../app/controllers/poll.server.controller.dynamo');

	// Polls Routes
	app.route('/ip')
		.get(polls.ip)

	app.route('/polls')
		.get(polls.list)
		.post(users.requiresLogin, polls.create);

    app.route('/vote/:pollId')
    	.post(polls.vote);

	app.route('/polls/:pollId')
		.get(polls.read)
		.put(users.requiresLogin, polls.hasAuthorization, polls.update)
		.delete(users.requiresLogin, polls.hasAuthorization, polls.delete);

	// Finish by binding the Quest middleware
	app.param('pollId', polls.pollByID);

};*/
