/*'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller.dynamo');
	var	banners = require('../../app/controllers/banners.server.controller.dynamo');

	// Banners Routes
	app.route('/banners')
		.get(banners.listClient)
		.put(users.requiresLogin, banners.hasAuthorization, banners.update)
		.post(users.requiresLogin, banners.create);
	app.route('/banners/search')
		.get(banners.search);

	app.route('/banners/:bannerId')
		.get(banners.read)
		.put(users.requiresLogin, banners.hasAuthorization, banners.update)
		.delete(users.requiresLogin, banners.hasAuthorization, banners.delete);

	// Finish by binding the Banner middleware
	app.param('bannerId', banners.bannerByID);
};*/
