/*'use strict';


var passport = require('passport');

module.exports = function(app) {
	// User Routes
	var users = require('../../app/controllers/users.server.controller.dynamo');

	// Setting up the users profile api
	app.route('/users/me').get(users.me);
	app.route('/user/:userId').get(users.byuser);
	app.route('/users').put(users.update);
	//app.route('/users/accounts').delete(users.removeOAuthProvider);

	// Setting up the users password api
	app.route('/users/password').post(users.changePassword);
	app.route('/auth/forgot').post(users.forgot);
	app.route('/auth/reset/:token/:user').get(users.validateResetToken);
	app.route('/auth/reset/:token/:user').post(users.reset);

	// Setting up the users authentication api

	app.route('/auth/signup').post(users.requiresLogin,users.signup);
	app.route('/auth/signin').post(users.signin);
	app.route('/auth/signout').get(users.signout);

	// Finish by binding the user middleware
	app.param('userId', users.userByID);
};*/
