'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

/**
 * Extend user's controller
 */
module.exports = _.extend(
	require('./users/users.authentication.server.controller.dynamo'),
	require('./users/users.authorization.server.controller.dynamo'),
	require('./users/users.password.server.controller.dynamo'),
	require('./users/users.profile.server.controller.dynamo')
);
