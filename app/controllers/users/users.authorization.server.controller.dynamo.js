'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');
var AWS = require('aws-sdk');
AWS.config.update({
	region: "us-east-1"
});

var docClient = new AWS.DynamoDB.DocumentClient();
var generate = require('node-uuid');

/**
 * User middleware
 */
exports.userByID = function(req, res, next, id) {

	var params = {
		TableName: 'User',
			Key:{
				username:id
			}
	};

	docClient.get(params, function(err, data){

			if(err) res.json(err);
			req.profile = data.Item;
			next();

	});
};

exports.byuser = function(req, res) {

	if(req.profile != undefined){
		var profile = {
			firstName: req.profile.firstName,
			lastName: req.profile.lastName,
			displayName: req.profile.displayName,
			images:req.profile.images,
			username:req.profile.username,
			meta:req.profile.meta
		}

	}else{
		var profile = null;
	}

	res.json(profile || null);
};

/**
 * Require login routing middleware
 */
exports.requiresLogin = function(req, res, next) {
	if (!req.isAuthenticated()) {
		return res.status(401).send({
			message: 'User is not logged in'
		});
	}
		next();
};

/**
 * User authorizations routing middleware
 */
exports.hasAuthorization = function(roles) {
	var _this = this;

	return function(req, res, next) {
		_this.requiresLogin(req, res, function() {
			if (_.intersection(req.user.roles, roles).length) {
				return next();
			} else {
				return res.status(403).send({
					message: 'User is not authorized'
				});
			}
		});
	};
};
