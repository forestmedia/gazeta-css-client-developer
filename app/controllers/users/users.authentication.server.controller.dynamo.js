'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	passAuth = require('../../models/user.server.dynamo'),
	errorHandler = require('../errors.server.controller'),
	passport = require('passport');


	var AWS = require('aws-sdk');
	AWS.config.update({
		region: "us-east-1"
	});

	var docClient = new AWS.DynamoDB.DocumentClient();
	var generate = require('node-uuid');

/**
 * Signup
 */
exports.signup = function(req, res) {

	// For security measurement we remove the roles from the req.body object
	delete req.body.roles;

	// Init Variables
	var user = req.body;
	var message = null;

	var pre = passAuth.pre(user.password);
	user.password = pre.password;
	user.salt = pre.salt;


	// Add missing user fields
	user.provider = 'local';
	user.displayName = user.firstName + ' ' + user.lastName;


	var params = {

		TableName: "User",
				// Insert request por body to item params
				Item: user
	};

	//Save User
	docClient.put(params, function(err) {

			if (err) { // an error occurred
					res.status(404);
					res.json(JSON.stringify(err, null, 2));
			}
			else { // successful response
				// Remove sensitive data before login
				user.password = undefined;
				user.salt = undefined;

				req.login(user, function(err) {
					if (err) {
						res.status(400).send(err);
					} else {
						res.json(user);
					}
				});
			}
	});
};


/**
 * Signin after passport authentication
 */
exports.signin = function(req, res, next) {
	passport.authenticate('local', function(err, user, info) {
		if (err || !user) {
			res.status(400).send(info);
		} else {
			// Remove sensitive data before login
			user.password = undefined;
			user.salt = undefined;

			req.login(user, function(err) {
				if (err) {
					res.status(400).send(err);
				} else {
					res.json(user);
				}
			});
		}
	})(req, res, next);
};

/**
 * Signout
 */
exports.signout = function(req, res) {
	req.logout();
	res.redirect('/');
};

/**
 * OAuth callback
 */
exports.oauthCallback = function(strategy) {
	return function(req, res, next) {
		passport.authenticate(strategy, function(err, user, redirectURL) {
			if (err || !user) {
				return res.redirect('/#!/signin');
			}
			req.login(user, function(err) {
				if (err) {
					return res.redirect('/#!/signin');
				}

				return res.redirect(redirectURL || '/');
			});
		})(req, res, next);
	};
};


function create(user) {

	var message = null;

	// Add missing user fields
	user.provider = 'local';
	user.displayName = user.firstName + ' ' + user.lastName;

	var pre = passAuth.pre(user.password);
	user.password = pre.password;
	user.salt = pre.salt;

	var params = {

		TableName: "User",
				// Insert request por body to item params
				Item: user
	};

	//Save User
	docClient.put(params, function(err) {

			if (err) { // an error occurred
					console.error(err);
			}
			else { // successful response

			}
	});
};



var container = {

provider: 'local',
username: 'anto2318',
password:'viajes4000',
__v: 0,
image: 'v1427130764/gz/10895482_1568087676778480_1623352445_n.jpg',
updated: Date.now,
created: Math.floor(Date.now()) ,
roles: [ 'admin' ],
meta:
 { description: 'Esto es una prueba de perfil',
   facebook: 'joseluisredri',
   twitter: 'akolorfilms',
   instagram: 'a',
   website: 'www.bobjargon.com' },
images: {},
email: 'anto2318@gmail.com',
lastName: 'Nardi',
firstName: 'Antonio'
};

create(container);
