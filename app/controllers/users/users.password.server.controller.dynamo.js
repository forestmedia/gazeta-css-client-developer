'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	passAuth = require('../../models/user.server.dynamo'),
	errorHandler = require('../errors.server.controller'),
	passport = require('passport'),
	config = require('../../../config/config'),
	nodemailer = require('nodemailer'),
	async = require('async'),
	crypto = require('crypto');

	var AWS = require('aws-sdk');
	AWS.config.update({
		region: "us-east-1"
	});

	var docClient = new AWS.DynamoDB.DocumentClient();
	var generate = require('node-uuid');

/**
 * Forgot for reset password (forgot POST)
 */
exports.forgot = function(req, res, next) {
	async.waterfall([
		// Generate random token
		function(done) {
			crypto.randomBytes(20, function(err, buffer) {
				var token = buffer.toString('hex');
				done(err, token);
			});
		},
		// Lookup user by username
		function(token, done) {
			var user = {};
			if (req.body.username) {

				var params = {
					TableName: 'User',
						Key:{
							username:req.body.username
						}
				};

				docClient.get(params, function(err, data){
						if(err) done(err);
						user = data.Item;

					if (!user) {
						return res.status(400).send({
							message: 'No account with that username has been found'
						});
					} else {
						user.resetPasswordToken = token;
						user.resetPasswordExpires = Date.now() + 3600000; // 1 hour


						var par = {

							TableName: "User",
									// Insert request por body to item params
									Item: user
						};

						docClient.put(par, function(err){
							done(err, token, user);
						});
					//	next();
					}

					});

			} else {
				return res.status(400).send({
					message: 'Username field must not be blank'
				});
			}
		},
		function(token, user, done) {
			res.render('templates/reset-password-email', {
				name: user.displayName,
				appName: config.app.title,
				url: 'http://' + req.headers.host + '/auth/reset/' + token + '/' + user.username
			}, function(err, emailHTML) {
				done(err, emailHTML, user);
			});
		},
		// If valid email, send reset email using service
		function(emailHTML, user, done) {

			var smtpTransport = nodemailer.createTransport(config.mailer.options);
			var mailOptions = {
				to: user.email,
				from: config.mailer.from,
				subject: 'Password Reset',
				html: emailHTML
			};

			smtpTransport.sendMail(mailOptions, (err) => {
				if (!err) {
					res.status(200).send({
						message: 'An email has been sent to your email with further instructions.'
					});
				}
				done(err);
			});
		}
	], (err) => {
		if (err) return next(err);
	});
};

/**
 * Reset password GET from email token
 */
exports.validateResetToken = function(req, res) {



	var params = {

		TableName: 'User',
			Key:{
				username:req.params.user
			}

	};

	docClient.get(params, function(err, data){
		var user = data.Item;

		var dateNow = Date.now();
				if(err) res.json(err);
				else{
					if (req.params.token == user.resetPasswordToken && user.resetPasswordExpires > dateNow) {

													res.redirect('/#!/password/reset/' + req.params.token + '/' + req.params.user);
					}else{
									return res.redirect('/#!/password/reset/invalid');
					}

				}
			});
	}


/**
 * Reset password POST from email token
 */

exports.reset = function(req, res, next) {

	// Init Variables
	var passwordDetails = req.body;

	async.waterfall([

		function(done) {

			var params = {

				TableName: 'User',
					Key:{
						username:req.params.user
					}

			};

			docClient.get(params, function(err, data){
				var user = data.Item;
				var dateNow = Date.now();

						if(err) res.json(err);
						else{

							if (req.params.token == user.resetPasswordToken && user.resetPasswordExpires > dateNow) {

															if (passwordDetails.newPassword === passwordDetails.verifyPassword) {

																		user.password = passAuth.pre(passwordDetails.newPassword);
																		user.resetPasswordToken = undefined;
																		user.resetPasswordExpires = undefined;

																		var par = {

																			TableName: "User",
																					// Insert request por body to item params
																					Item: user
																		};

																		//Save User
																		docClient.put(par, function(err) {

																				if (err) { // an error occurred
																						res.status(404);
																						res.json(JSON.stringify(err, null, 2));
																				}
																				else { // successful response
																					req.login(user, function(err) {
																						if (err) {
																							res.status(400).send(err);
																						} else {
																							// Return authenticated user
																							res.json(user);

																							done(err, user);
																						}
																					});
																				}//else end
																		});

															} else {

																return res.status(400).send({
																	message: 'Passwords do not match'
																});

															}
							}else{
								return res.status(400).send({
									message: 'Password reset token is invalid or has expired.'
								});
							}

						}
					})
				},
					function(user, done) {
						res.render('templates/reset-password-confirm-email', {
							name: user.displayName,
							appName: config.app.title
						}, function(err, emailHTML) {
							done(err, emailHTML, user);
						});
					},
					// If valid email, send reset email using service
					function(emailHTML, user, done) {
						var smtpTransport = nodemailer.createTransport(config.mailer.options);
						var mailOptions = {
							to: user.email,
							from: config.mailer.from,
							subject: 'Your password has been changed',
							html: emailHTML
						};

						smtpTransport.sendMail(mailOptions, function(err) {
							done(err, 'done');
						});
					}
				], function(err) {
					if (err) return next(err);
				});

};

/**
 * Change Password
 */
exports.changePassword = function(req, res) {
	// Init Variables
	var passwordDetails = req.body;

	if (req.user) {
		if (passwordDetails.newPassword) {
			console.log(req.user);
			var params = {

				TableName: 'User',
					Key:{
						username:req.user.username
					}

			};

			docClient.get(params, function(err, data){

				if (err || !data.Item) res.status(400).send({
					message: 'User is not found'
				});

				var user = data.Item;
				if (passAuth.authenticate(passwordDetails.currentPassword, req.user)) {
					if (passwordDetails.newPassword === passwordDetails.verifyPassword) {

						user.password = passAuth.pre(passwordDetails.newPassword);

						var par = {

							TableName: "User",
									// Insert request por body to item params
									Item: user
						};

						//Save Password
						docClient.put(par, function(err) {

							if (err) {
								return res.status(400).send({
									message: errorHandler.getErrorMessage(err)
								});
							} else {
								req.login(user, function(err) {
									if (err) {
										res.status(400).send(err);
									} else {
										res.send({
											message: 'Password changed successfully'
											});
										}
									});
								}

							})

						}else {
							res.status(400).send({
								message: 'Passwords do not match'
							});
						}

					}else {
						res.status(400).send({
							message: 'Current password is incorrect'
						});
					}
				});
			}else{

					res.status(400).send({
						message: 'Please provide a new password'
					});
			}
		}else {
			res.status(400).send({
				message: 'User is not signed in'
			});
		}
};
