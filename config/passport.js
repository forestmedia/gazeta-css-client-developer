'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport'),
	path = require('path'),
	config = require('./config');

	var AWS = require('aws-sdk');
	AWS.config.update({
		region: "us-east-1"
	});

	var docClient = new AWS.DynamoDB.DocumentClient();
	var generate = require('node-uuid');

/**
 * Module init function.
 */
module.exports = function() {
	// Serialize sessions
	passport.serializeUser(function(user, done) {
		done(null, user.username);
	});

	// Deserialize sessions
	passport.deserializeUser(function(id, done) {

		var params = {

			TableName: 'User',
				Key:{
					username:id
				}

		};

		docClient.get(params, function(err, data){
				var user = data.Item;
				done(err, user);
		});
});

	// Initialize strategies
	config.getGlobbedFiles('./config/strategies/**/*.js').forEach(function(strategy) {
		require(path.resolve(strategy))();
	});
};
