'use strict';

/**
 * Module dependencies.
 */
var glob = require('glob'),
	chalk = require('chalk');

/**
 * Module init function.
 */
module.exports = function() {

	/**
	 * Before we begin, lets set the environment variable
	 * We'll Look for a valid NODE_ENV variable and if one cannot be found load the development NODE_ENV
	 */

	if(process.env.NODE_ENV == undefined){
		process.env.NODE_ENV = 'development';
	}

	var files = glob.sync('./config/env/' + process.env.NODE_ENV + '.js');
	console.log(glob.sync('./config/env/' + process.env.NODE_ENV + '.js'));


};
