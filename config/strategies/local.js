'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy,
	passAuth = require('../../app/models/user.server.dynamo.js');

	var AWS = require('aws-sdk');
	AWS.config.update({
		region: "us-east-1"
	});

	var docClient = new AWS.DynamoDB.DocumentClient();
	var generate = require('node-uuid');


module.exports = function() {

	// Use local strategy
	passport.use(new LocalStrategy({
			usernameField: 'username',
			passwordField: 'password'
		},
		function(username, password, done) {

			var params = {

				TableName: 'User',
					Key:{
						username:username
					}

			};

			docClient.get(params, function(err, data){
				var user = data.Item;

				if(err) res.json(err);
				else{
					if (!user) {
						return done(null, false, {
							message: 'Unknown user or invalid password'
						});
					}

					if (!passAuth.authenticate(password, user)) {
						return done(null, false, {
							message: 'Unknown user or invalid password'
						});
					}
				}
					return done(null, user);
			})
		}
	));
};
