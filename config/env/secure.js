'use strict';

module.exports = {
	port: 443,
//	db: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://localhost/gccs',
	assets: {
		lib: {
			css: [
				'public/dist/vendor.css'
			],
			js: [
				'public/dist/vendor.min.js'
			]
		},
		css:[
			'public/dist/application.min.css'
		],
		js: [
			'public/dist/application.min.js'
		]

	},
	mailer: {
		from: process.env.MAILER_FROM || 'anto2318@gmail.com',
		options: {
			port: 465,
			secure: true, // use SSL
			host: process.env.MAILER_SERVICE_PROVIDER || 'smtp.gmail.com',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'anto2318',
				pass: process.env.MAILER_PASSWORD || 'viajes4000'
			}
		}
	}
};
