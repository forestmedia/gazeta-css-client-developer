'use strict';

module.exports = {
	app: {
		title: 'Gazeta de Caracas',
		description: 'Noticiero Digital, de Venezuela y el Mundo.',
		keywords: 'gaceta, gazeta, noticias, caracas, venezuela, noticiero, digital, mundo'
	},
	port: process.env.PORT || 9000,
	templateEngine: 'swig',
	sessionSecret: 'MEAN',
	//sessionCollection: 'sessions',
	assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.css',
				'public/lib/bootstrap/dist/css/bootstrap-theme.css',
				'public/lib/angular-tags/dist/angular-tags-0.3.1.css',
				'public/lib/ng-twitter/src/ng-twitter.css',
				//'public/lib/textAngular/dist/textAngular.css',
				'public/lib/font-awesome-4.4.0/css/font-awesome.css'
			],
			js: [
				'public/lib/angular/angular.js',
				'public/lib/angular/i18n/angular-locale_es-ve.js',
				'public/lib/angular/angular-resource.js',
				'public/lib/angular/angular-cookies.js',
				'public/lib/angular/angular-animate.js',
				'public/lib/angular/angular-touch.js',
				'public/lib/angular/angular-sanitize.js',
				'public/lib/clipboard/dist/clipboard.js',
				//'public/lib/ngclipboard/dist/ngclipboard.js',
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls-1.3.3.js',
				'public/lib/moment/moment.js',
				'public/lib/moment/locale/es.js',
				'public/lib/angular-moment/angular-moment.js',
				'public/lib/angular-aside-master/dist/js/angular-aside.js',
				//'public/lib/angular-tags/dist/angular-tags-0.3.1.js',
				'public/lib/angular-infinite/ng-infinite-scroll.js',
				'public/lib/angular-loading-bar/build/loading-bar.js',
				'public/lib/ng-twitter/src/ng-twitter.js',
				'public/lib/angular-disqus/angular-disqus.js',
				'public/lib/angular-ga/ga.js',
				//'public/lib/angular-file-upload-master/dist/angular-file-upload.js',
				//'public/lib/textAngular/dist/textAngular-sanitize.js',
				//'public/lib/textAngular/dist/textAngular-rangy.min.js',
				//'public/lib/textAngular/dist/textAngular.js',
				//'public/lib/textAngular/dist/textAngularSetup.js',
				'public/lib/stickyfill-master/dist/stickyfill.js',
				//'public/lib/angular-drag-and-drop/angular-drag-and-drop-lists.js',
				'public/lib/socket/socket.io-1.4.5.js'
			]
		},
		css: [
			'public/modules/**/css/*.css'

		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js'
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};
