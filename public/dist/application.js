'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
	// Init module configuration options
	var applicationModuleName = 'gccs';
	var applicationModuleVendorDependencies = ['ngResource', 'ngCookies',  'ngAnimate',  'ngTouch',  'ngSanitize',  'ui.router', 'templates-main' ,'ui.bootstrap','ui.bootstrap.typeahead','ui.bootstrap.modal' ,'ui.utils','angularMoment','infinite-scroll','angular-loading-bar','ngDisqus','ga','ngAside'];

	// Add a new vertical module
	var registerModule = function(moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})(); 

'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.hashPrefix('!');
	}
]);

// Setting HTML5 Location Mode

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');
'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider','cfpLoadingBarProvider',


	function($stateProvider, $urlRouterProvider, cfpLoadingBarProvider) {

		cfpLoadingBarProvider.includeBar = true;

		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html',
			controller:'HomeController'

		}).
		state('viewCategory', {
			url: '/noticias/:tipo/:filtro',
			templateUrl: 'modules/core/views/category.client.view.html',
			controller:'HomeController'

		}).
		state('viewUser', {
			url: '/:filtro',
			templateUrl: 'modules/core/views/category.client.view.html',
			controller:'ByUserController'
		}).
		state('viewTV', {
			url: '/noticias/:tipo',
			templateUrl: 'modules/core/views/tv.client.view.html',
			controller:'TvController'

		}).
		state('busqueda', {
			url: '/busqueda/:filtro',
			templateUrl: 'modules/core/views/busqueda.client.view.html',
			controller:'HomeController'
		}).
		state('busquedaTipo', {
			url: '/busqueda/:filtro/:tipo',
			templateUrl: 'modules/core/views/busqueda.client.view.html',
			controller:'HomeController'
		}).
		state('articleViewPublic', {
			url: '/articulo/:articuloId/:articuloDate',
			templateUrl: 'modules/core/views/article.client.view.html',
			controller:'HomeController'
		});

	}
]);

angular.module('core').run(['amMoment','$rootScope', '$http' ,function(amMoment, $rootScope, $http) {
    amMoment.changeLocale('es');

		$rootScope.modelo = {
			articles : {},
			banners : {},
			Política: {},
			Economía:{},
			Sucesos:{},
			Opinión:{},
			Imagen:{},
			update_date:"",
			widgets:{
		    tv :[],
		    poll:[],
		    custom:[],
		    likes:[],
		    popular:[],
		    hashtag:[],
		    user:[]
		  }
		}


		$rootScope.option = [
			  { label: 'Política', value: 0 },
			  { label: 'Economía', value: 1 },
			  { label: 'Sucesos', value: 2 },
			  { label: 'Opinión', value: 3 },
				{ label: 'Imagen', value: 4}
			];

		var Socket = {
		 socket: io.connect('http://gazeta-admin.us-east-1.elasticbeanstalk.com:8022')
	 	};

		$rootScope.isReady = false;

		Socket.socket.on('notification', function(response) {
				 $rootScope.modelo = response;
				 $rootScope.isReady = true;
				 console.log(response);
				 $rootScope.$broadcast('modelReady');
		});



}]);

angular.module('core').filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});

angular.module('templates-main', ['modules/core/views/article.client.view.html', 'modules/core/views/aside.html', 'modules/core/views/busqueda.client.view.html', 'modules/core/views/category.client.view.html', 'modules/core/views/customTemplate.html', 'modules/core/views/footer.client.view.html', 'modules/core/views/header.client.view.html', 'modules/core/views/home.client.view.html', 'modules/core/views/player.html', 'modules/core/views/tv.client.view.html', 'templates/banner-client-template.html', 'templates/box-client-template.html', 'templates/category-client-template.html', 'templates/chart-client-template.html', 'templates/encuesta-client-template.html', 'templates/hash-client-template.html', 'templates/image-day-client-template.html', 'templates/lasted-client-template.html', 'templates/miniplayer-client-template.html', 'templates/most-read-client-template.html', 'templates/newsletter-client-template.html', 'templates/sidebar-client-template.html', 'templates/tags.html', 'templates/top-client-template.html', 'templates/twitter-client-template.html', 'templates/uploadFile-client-template.html', 'templates/videos-client-template.html', 'templates/widgets-client-template.html', 'templates/youtube-client-template.html']);

angular.module("modules/core/views/article.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/core/views/article.client.view.html",
    "<section data-ng-init=findOne() ng-show=cargado><div class=\"col-xs-12 col-md-9 no-sp centering\"><div class=\"dn-line clearfix\"><box article=article tipo=3 size=700></box><div class=\"col-md-12 mg-top no-sp\"><banner banners=0></banner></div></div><h3 class=\"col-xs-12 col-md-12\">Artículos Relacionados</h3><div class=\"col-xs-12 col-md-12 no-sp dn-line clearfix\"><div class=\"col-xs-12 col-md-6\"><box article=article tipo=2></box></div><div class=\"col-xs-12 col-md-6\"><box article=article tipo=2></box></div><div class=\"col-xs-12 col-md-6\"><box article=article tipo=2></box></div><div class=\"col-xs-12 col-md-6\"><box article=article tipo=2></box></div></div></div><div class=\"col-xs-12 col-md-8 centering\"><h3 class=\"col-xs-12 col-md-12 no-sp\">Comentarios</h3><dir-disqus config=disqusConfig></dir-disqus></div></section>");
}]);

angular.module("modules/core/views/aside.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/core/views/aside.html",
    "<div class=modal-header><h3 class=modal-title>Gazeta de Caracas</h3></div><div class=modal-body><ul class=\"nav navbar-nav navbar-left menuli\"><li><a href=/#!/noticias/category/0 role=button aria-expanded=false ng-click=ok($event)>Política</a></li><li><a href=/#!/noticias/category/1 role=button aria-expanded=false ng-click=ok($event)>Economía</a></li><li><a href=/#!/noticias/category/2 role=button aria-expanded=false ng-click=ok($event)>Sucesos</a></li><li><a href=/#!/noticias/category/3 role=button aria-expanded=false ng-click=ok($event)>Opinión</a></li><li><a href=/#!/noticias/tv role=button aria-expanded=false ng-click=ok($event)>TV</a></li></ul></div>");
}]);

angular.module("modules/core/views/busqueda.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/core/views/busqueda.client.view.html",
    "<div class=\"col-xs-12 col-md-12\" data-ng-init=findT(10,false,queryVal) ng-controller=BusquedaController><section><div class=\"col-xs-11 col-md-11 centering\" style=margin-top:10px><input class=form-control data-ng-model=queryVal placeholder=Busqueda ng-change=\"findT(6,false, queryVal)\" auto-focus></div><div infinite-scroll=contenedor.nextPage() infinite-scroll-disabled=contenedor.busy||contenedor.stop infinite-scroll-distance=1 class=\"col-xs-12 col-sm-12 col-md-12 no-sp\"><div data-ng-repeat=\"article in contenedor.items\"><div class=\"col-xs-12 col-sm-12 col-md-12 clearfix\"><box article=article tipo=5></box></div></div><h2 ng-show=contenedor.busy><div class=loader style=\"margin-top:80px;font-size: 4px\"></div></h2><div ng-show=contenedor.stop class=clearfix><br></div><div class=col-xs-12 ng-show=contenedor.stop><div class=\"col-xs-12 col-md-12\" style=\"text-align:center; margin-top:15px;margin-bottom:20px; font-size:20px\">CONTACTO | PUBLICIDAD</div><h5 style=text-align:center>Copyright Gazeta de Caracas © 2016 Aviso legal. Todos los derechos reservados</h5></div></div></section></div>");
}]);

angular.module("modules/core/views/category.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/core/views/category.client.view.html",
    "<section ng-show=cargado><div class=\"col-md-12 col-xs-12\" ng-if=\"idcat == 1\"><lasted articles=articles tipo=idcat ng-if=\"idcat == 1\"></lasted></div><div class=col-md-12 ng-if=profile><h2>@{{profile.username}} <span>{{profile.displayName}}</span></h2><img ng-if=profile.images.header class=col-md-12 lazy-src=profile.images.header alt=\"profile.displayName\"></div><div class=\"row wrap\"><div class=\"col-xs-12 col-sm-12 col-md-12 no-sp\"><widget section={{option[idcat].label}} pos=Principal></widget><div data-ng-repeat=\"article in articles.results track by $index\"><div class=\"col-xs-12 col-sm-12 col-md-12 clearfix\" style={{article.limpiar}}><box article=article tipo=1 ng-if=article.important></box><banner banners=banners_P1 ng-show=\"$index % 4 == 0 && $index != 0\"></banner><box article=article tipo=5 ng-if=!article.important></box></div></div><div infinite-scroll=contenedor.nextPage() infinite-scroll-disabled=contenedor.busy||contenedor.stop infinite-scroll-distance=1 class=\"col-xs-12 col-sm-12 col-md-12 no-sp\"><div data-ng-repeat=\"article in contenedor.items\"><div class=\"col-xs-12 col-sm-12 col-md-12 clearfix\" style={{article.limpiar}}><box article=article tipo=1 ng-if=article.important></box><banner banners=banners_P1 ng-show=\"$index % 4 == 0\"></banner><box article=article tipo=5 ng-if=!article.important></box></div></div><div ng-show=contenedor.stop class=clearfix><br></div></div><div class=\"col-md-12 mg-top no-sp\"><banner banners=banners_A1></banner><h2 ng-show=contenedor.busy><div class=loader style=\"margin-top:80px;font-size: 4px\"></div></h2></div></div><div class=\"hidden-xs col-sm-5 col-md-4 sidebar\" ng-show=cargado><div class=\"col-xs-12 col-sm-12 col-md-12 no-sp\" style=\"margin-bottom: -50px\"><widget section={{option[idcat].label}} pos=Lateral></widget></div><div class=\"col-xs-12 col-sm-12 col-md-12 no-sp sticky\" stickyfill top=80><div ng-if=profile><h2>@{{profile.username}}</h2><span>{{profile.displayName}}</span> <img class=col-md-12 lazy-src=profile.images.avatar alt=profile.displayName ng-if=\"profile.images.avatar\"><div class=sideuser ng-if=profile.meta.twitter><a class=sicon-tw href=https://www.twitter.com/{{profile.meta.twitter}}><span>@{{profile.meta.twitter}}</span></a></div><div class=sideuser ng-if=profile.meta.facebook><a class=sicon-f href=https://www.facebook.com/{{profile.meta.facebook}}><span>{{profile.meta.facebook}}</span></a></div><div class=sideuser ng-if=profile.meta.instagram><a class=sicon-g href=https://www.instagram.com/{{profile.meta.instagram}}><span>{{profile.meta.instagram}}</span></a></div><div class=sideuser ng-if=profile.meta.website><h4><a href={{profile.meta.website}}>{{profile.meta.website}}</a></h4></div></div><banner banners=banners_B1 ng-if=!profile></banner><div style=opacity:0.7><img style=width:100% lazy-src=modules/core/img/gazeta-svg/gz-ccs-home.svg><p style=text-align:center>Copyright © 2016 Aviso legal.<br>Todos los derechos reservados</p></div></div></div></div><section></section></section>");
}]);

angular.module("modules/core/views/customTemplate.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/core/views/customTemplate.html",
    "<a href=/#!/articulo/{{match.model.slug}}/{{match.model.published_date}} ng-controller=HeaderController ng-click=changeSearch()><span bind-html-unsafe=\"match.model.title | uibTypeaheadHighlight:query\">{{match.model.title}}</span> , fecha:{{match.model.created | date:'dd-MM-yyyy h:mm a'}}</a>");
}]);

angular.module("modules/core/views/footer.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/core/views/footer.client.view.html",
    "<div class=\"col-xs-12 centering\"><div class=\"col-xs-12 col-md-12\" style=\"text-align:center; margin-top:15px;margin-bottom:20px; font-size:20px\">CONTACTO | PUBLICIDAD</div><h5 style=text-align:center>Copyright Gazeta de Caracas © 2016 Aviso legal. Todos los derechos reservados</h5></div>");
}]);

angular.module("modules/core/views/header.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/core/views/header.client.view.html",
    "<div class=container data-ng-controller=HeaderController><div class=navbar-header><button class=\"navbar-toggle toggle-menu menu-right push-body\" type=button ng-click=\"openAside('left', true)\"><span class=sr-only></span> <span class=icon-bar></span> <span class=icon-bar></span> <span class=icon-bar></span></button> <button class=\"nav navbar-toggle no-bd\" ng-show=!searchView type=button><a role=button aria-expanded=false ng-click=inside();changeSearch();><span class=\"glyphicon glyphicon-search\"><span class=sr-only>Search</span></span></a></button> <a href=\"/#!/\" class=navbar-brand><img lazy-src=modules/core/img/gazeta-svg/gazeta-ccs-oscuro.svg></a></div><nav class=\"navbar-collapse collapse navbar-left\" collapse=!isCollapsed role=navigation><ul class=\"nav navbar-nav navbar-left hidden-md hidden-lg hidden-sm\"><li><a href=/#!/noticias/category/0 role=button aria-expanded=false>Política</a></li><li><a href=/#!/noticias/category/1 role=button aria-expanded=false>Economía</a></li><li><a href=/#!/noticias/category/2 role=button aria-expanded=false>Sucesos</a></li><li><a href=/#!/noticias/category/3 role=button aria-expanded=false>Opinión</a></li><li><a href=/#!/noticias/tv role=button aria-expanded=false>TV</a></li></ul><ul class=\"nav navbar-nav navbar-left\"><li ng-repeat=\"tag in tags\"><a href=/#!/busqueda/{{tag}}/tags role=button aria-expanded=false><span>#</span>{{tag}}</a></li></ul></nav><ul class=\"nav navbar-nav navbar-collapse collapse navbar-right\"><li ng-show=!searchView><a role=button aria-expanded=false ng-click=inside();changeSearch();><span class=\"glyphicon glyphicon-search\"><span class=sr-only>Search</span></span></a></li><li><button class=\"navbar-toggle toggle-menu menu-right push-body inh\" type=button ng-click=\"openAside('left', true)\"><span class=sr-only></span> <span class=icon-bar></span> <span class=icon-bar></span> <span class=icon-bar></span></button></li></ul><form class=\"navbar-form navbar-right\" role=search ng-show=searchView style=width:100%><div class=input-group style=width:100% ng-click=inside()><input ng-model=customSelected placeholder=Busqueda class=form-control> <span class=input-group-btn style=width:80px><button class=\"btn no-rd\" ng:show=!!isLoading><img lazy-src=modules/core/img/loaders/loading_4-1.gif></button> <button type=reset class=\"btn no-rd\" ng-click=changeSearch()><span class=\"glyphicon glyphicon-remove\"><span class=sr-only>Close</span></span></button> <button class=\"btn no-rd\" ng-click=\"onClick(customSelected);changeSearch();clickChange('busqueda')\"><span class=\"glyphicon glyphicon-search\"><span class=sr-only>Search</span></span></button></span></div></form></div>");
}]);

angular.module("modules/core/views/home.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/core/views/home.client.view.html",
    "<section ng-show=cargado><div class=\"col-md-12 col-xs-12\"><lasted></lasted></div><div class=\"col-md-12 col-xs-12 tope\" ng-controller=box-controller ng-if=\"articles.results[0].meta.important > 0\"><div class=\"col-xs-12 col-sm-12 col-md-12 no-sp\"><box tipo=4 size=1020 article=articles.results[0]></box></div><div class=\"col-md-12 mg-top no-sp\"><banner banners=banners_A1></banner></div></div><div class=\"col-xs-12 col-sm-12 col-md-12 no-sp tope clearfix\" ng-if=\"articles.results[0].meta.important == 0\"><div class=\"col-xs-12 col-sm-8 col-md-8\"><box article=articles.results[0] tipo=1></box></div><div class=\"col-xs-12 col-sm-4 col-md-4\"><box article=articles.results[1] tipo=1></box></div><div class=\"col-md-12 mg-top\"><banner banners=banners_A1></banner></div></div><div class=\"col-xs-12 hidden-sm hidden-md hidden-lg\" ng-show=cargado><widget section=Home pos=Lateral></widget></div><div class=\"row wrap\"><div class=\"col-xs-12 col-sm-12 col-md-12\"><div data-ng-repeat=\"article in articles.results | startFrom : 2 | limitTo: 6 | orderBy:['meta.important','published_date']\"><div class=\"col-xs-12 col-sm-12 col-md-12 bl clearfix no-sp\" ng-show=!$first><box article=article tipo=1 ng-show=\"article.meta.important > 0\"></box><box article=article tipo=5 ng-show=\"article.meta.important == 0\"></box></div></div><div class=\"col-md-12 mg-top no-sp\"><banner banners=banners_P1></banner></div><widget section=Home pos=Principal></widget><div data-ng-repeat=\"article in articles.results | startFrom : 8 |  orderBy:['meta.important','published_date']\"><div class=\"col-xs-12 col-sm-12 col-md-12 bl clearfix no-sp\"><box article=article tipo=1 ng-show=\"article.meta.important > 0\"></box><banner banners=banners_P1 ng-show=\"$index % 4 == 0\"></banner><box article=article tipo=5 ng-show=\"article.meta.important == 0\"></box></div></div><div infinite-scroll=reddit.nextPage() infinite-scroll-disabled=reddit.busy||reddit.stop infinite-scroll-distance=1 class=\"col-xs-12 col-sm-12 col-md-12 no-sp\"><div data-ng-repeat=\"article in reddit.items\" class=\"col-xs-12 col-sm-12 col-md-12 no-sp bl clearfix\"><box article=article tipo=1 ng-show=article.meta.important></box><banner banners=banners_P1 ng-show=\"$index % 4 == 0\"></banner><box article=article tipo=5 ng-show=!article.meta.important></box></div></div><div class=\"col-md-12 mg-top no-sp\"><h2 ng-show=reddit.busy><div class=loader style=\"margin-top:80px;font-size: 4px\"></div></h2></div><div class=\"alert alert-warning text-center\" data-ng-if=\"articles.results.$resolved && !articles.results.length\">No se encontraron publicaciones</div></div><div class=\"hidden-xs col-sm-5 col-md-4 sidebar\" style=margin-top:50px ng-show=cargado><div class=\"col-xs-12 col-sm-12 col-md-12 no-sp\" style=\"margin-bottom: -90px\"><widget section=Home pos=Lateral></widget></div><div class=\"col-xs-12 col-sm-12 col-md-12 no-sp sticky\" stickyfill top=80><div><newsletter></newsletter><banner banners=banners_B1></banner><div style=opacity:0.7><img style=width:100% lazy-src=modules/core/img/gazeta-svg/gz-ccs-home.svg><p style=text-align:center>Copyright © 2016 Aviso legal<br>Todos los derechos reservados</p></div></div></div></div></div><miniplayer mini=Player title=\"EN VIVO: La tierra desde satelite.\"></miniplayer></section>");
}]);

angular.module("modules/core/views/player.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/core/views/player.html",
    "<youtube video-id=playVideo></youtube>");
}]);

angular.module("modules/core/views/tv.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/core/views/tv.client.view.html",
    "<section ng-init=initVideos()><br><div class=\"col-xs-12 col-md-10 no-sp centering\"><youtube auto-play=1 video-id=Player width=1280 height=720></youtube><h2>LIVESTREAM - Outer Space</h2></div><div class=\"col-md-12 no-sp\"><h2 style=margin-left:8px>Buen Provecho</h2><div class=contenedor><div class=\"row hero\"><div ng-repeat=\"video in videosTV1.data.items\"><div class=\"col-xs-1 col-md-1 no-sp view view-first\" ng-show=!activo ng-click=clickPic()><img id=imagen lazy-src={{video.snippet.thumbnails.medium.url}} class=\"col-xs-12 col-md-12 delay no-sp\"></div><div class=\"col-xs-1 col-md-1 no-sp view view-first\" ng-show=activo ng-mouseleave=clickPic()><img lazy-src={{video.snippet.thumbnails.medium.url}} class=\"col-xs-12 col-md-12 delay no-sp\"><div class=\"mask delay\"><h2>{{video.snippet.title}}</h2><p class=\"black delay\"></p><a data-ng-click=\"clickVideo(video.snippet.resourceId.videoId, video.snippet.title)\" class=\"info delay\">PLAY</a></div></div></div></div></div><h2 style=margin-left:8px>Expedientes</h2><div class=contenedor><div class=\"row hero\"><div ng-repeat=\"video in videosTV2.data.items\"><div class=\"col-xs-1 col-md-1 no-sp view view-first\" ng-show=!activo ng-click=clickPic()><img lazy-src={{video.snippet.thumbnails.medium.url}} class=\"col-xs-12 col-md-12 delay no-sp\"></div><div class=\"col-xs-1 col-md-1 no-sp view view-first\" ng-show=activo ng-mouseleave=clickPic()><img lazy-src={{video.snippet.thumbnails.medium.url}} class=\"col-xs-12 col-md-12 delay no-sp\"><div class=\"mask delay\"><h2>{{video.snippet.title}}</h2><p class=\"black delay\"></p><a data-ng-click=\"clickVideo(video.snippet.resourceId.videoId, video.snippet.title)\" class=\"info delay\">PLAY</a></div></div></div></div></div><h2 style=margin-left:8px>Política</h2><div class=contenedor><div class=\"row hero\"><div ng-repeat=\"video in videosTV3.data.items\"><div class=\"col-xs-1 col-md-1 no-sp view view-first\" ng-show=!activo ng-click=clickPic()><img lazy-src={{video.snippet.thumbnails.medium.url}} class=\"col-xs-12 col-md-12 delay no-sp\"></div><div class=\"col-xs-1 col-md-1 no-sp view view-first\" ng-show=activo ng-mouseleave=clickPic()><img lazy-src={{video.snippet.thumbnails.medium.url}} class=\"col-xs-12 col-md-12 delay no-sp\"><div class=\"mask delay\"><h2>{{video.snippet.title}}</h2><p class=\"black delay\"></p><a data-ng-click=\"clickVideo(video.snippet.resourceId.videoId, video.snippet.title)\" class=\"info delay\">PLAY</a></div></div></div></div></div><h2 style=margin-left:8px>Economía</h2><div class=contenedor><div class=\"row hero\"><div ng-repeat=\"video in videosTV4.data.items\"><div class=\"col-xs-1 col-md-1 no-sp view view-first\" ng-show=!activo ng-click=clickPic()><img lazy-src={{video.snippet.thumbnails.medium.url}} class=\"col-xs-12 col-md-12 delay no-sp\"></div><div class=\"col-xs-1 col-md-1 no-sp view view-first\" ng-show=activo ng-mouseleave=clickPic()><img lazy-src={{video.snippet.thumbnails.medium.url}} class=\"col-xs-12 col-md-12 delay no-sp\"><div class=\"mask delay\"><h2>{{video.snippet.title}}</h2><p class=\"black delay\"></p><a data-ng-click=\"clickVideo(video.snippet.resourceId.videoId, video.snippet.title)\" class=\"info delay\">PLAY</a></div></div></div></div></div><h2 style=margin-left:8px>Sucesos</h2><div class=contenedor><div class=\"row hero\"><div ng-repeat=\"video in videosTV5.data.items\"><div class=\"col-xs-1 col-md-1 no-sp view view-first\" ng-show=!activo ng-click=clickPic()><img lazy-src={{video.snippet.thumbnails.medium.url}} class=\"col-xs-12 col-md-12 delay no-sp\"></div><div class=\"col-xs-1 col-md-1 no-sp view view-first\" ng-show=activo ng-mouseleave=clickPic()><img lazy-src={{video.snippet.thumbnails.medium.url}} class=\"col-xs-12 col-md-12 delay no-sp\"><div class=\"mask delay\"><h2>{{video.snippet.title}}</h2><p class=\"black delay\"></p><a data-ng-click=\"clickVideo(video.snippet.resourceId.videoId, video.snippet.title)\" class=\"info delay\">PLAY</a></div></div></div></div></div><h2 style=margin-left:8px>Opinión</h2><div class=contenedor><div class=\"row hero\"><div ng-repeat=\"video in videosTV6.data.items\"><div class=\"col-xs-1 col-md-1 no-sp view view-first\" ng-show=!activo ng-click=clickPic()><img lazy-src={{video.snippet.thumbnails.medium.url}} class=\"col-xs-12 col-md-12 delay no-sp\"></div><div class=\"col-xs-1 col-md-1 no-sp view view-first\" ng-show=activo ng-mouseleave=clickPic()><img lazy-src={{video.snippet.thumbnails.medium.url}} class=\"col-xs-12 col-md-12 delay no-sp\"><div class=\"mask delay\"><h2>{{video.snippet.title}}</h2><p class=\"black delay\"></p><a data-ng-click=\"clickVideo(video.snippet.resourceId.videoId, video.snippet.title)\" class=\"info delay\">PLAY</a></div></div></div></div></div><h2 style=margin-left:8px>Un Café con Carla</h2><div class=contenedor><div class=\"row hero\"><div ng-repeat=\"video in videosTV7.data.items\"><div class=\"col-xs-1 col-md-1 no-sp view view-first\" ng-show=!activo ng-click=clickPic()><img lazy-src={{video.snippet.thumbnails.medium.url}} class=\"col-xs-12 col-md-12 delay no-sp\"></div><div class=\"col-xs-1 col-md-1 no-sp view view-first\" ng-show=activo ng-mouseleave=clickPic()><img lazy-src={{video.snippet.thumbnails.medium.url}} class=\"col-xs-12 col-md-12 delay no-sp\"><div class=\"mask delay\"><h2>{{video.snippet.title}}</h2><p class=\"black delay\"></p><a data-ng-click=\"clickVideo(video.snippet.resourceId.videoId, video.snippet.title)\" class=\"info delay\">PLAY</a></div></div></div></div></div><h2 style=margin-left:8px>Noticias24</h2><div class=contenedor><div class=\"row hero\"><div ng-repeat=\"video in videosTV8.data.items\"><div class=\"col-xs-1 col-md-1 no-sp view view-first\" ng-show=!activo ng-click=clickPic()><img lazy-src={{video.snippet.thumbnails.medium.url}} class=\"col-xs-12 col-md-12 delay no-sp\"></div><div class=\"col-xs-1 col-md-1 no-sp view view-first\" ng-show=activo ng-mouseleave=clickPic()><img lazy-src={{video.snippet.thumbnails.medium.url}} class=\"col-xs-12 col-md-12 delay no-sp\"><div class=\"mask delay\"><h2>{{video.snippet.title}}</h2><p class=\"black delay\"></p><a data-ng-click=\"clickVideo(video.snippet.resourceId.videoId, video.snippet.title)\" class=\"info delay\">PLAY</a></div></div></div></div></div></div></section><div class=col-xs-12><div class=\"col-xs-12 col-md-12\" style=\"text-align:center; margin-top:15px;margin-bottom:20px; font-size:20px\">CONTACTO | PUBLICIDAD</div><h5 style=text-align:center>Copyright Gazeta de Caracas © 2016 Aviso legal. Todos los derechos reservados</h5></div>");
}]);

angular.module("templates/banner-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/banner-client-template.html",
    "<div ng-show=banner.image.banner class=ban><img class=\"col-xs-12 no-sp\" lazy-src={{banner.image.banner}}></div>");
}]);

angular.module("templates/box-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/box-client-template.html",
    "<article ng-if=\"$ctrl.tipo==1\"><div class=\"col-sm-4 col-md-4 col-xs-4 no-sp\"><div class=dn-sep>“</div></div><div class=\"col-sm-8 col-md-8 col-xs-8 no-sp\"><div class=dn-author>Autor: @<a ng-href=#!/{{::$ctrl.article.user.username}}>{{::$ctrl.article.user.username}}</a></div></div><div class=\"col-sm-12 col-md-12 col-xs-12 no-sp\"><div class=\"dn-head-meta dn-date-share\"><p class=no-mg><a data-ng-href=#!/noticias/category/{{::$ctrl.article.category}}>- {{::option[$ctrl.article.category].label}} -</a> <time class=times am-time-ago=$ctrl.article.published_date></time></p></div><div class=dn-share><a class=sicon-tw href=\"https://twitter.com/share?text={{::$ctrl.article.title}};url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'twitter-share', 'width=550,height=235');return false\"></a> <a class=sicon-f href=\"https://www.facebook.com/sharer/sharer.php?u=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'facebook-share','width=580,height=296');return false\"></a> <a class=sicon-g href=\"https://plus.google.com/share?url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'google-plus-share', 'width=490,height=530');return false\"></a></div><a data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><img class=\"titular-gz no-mg\" ng-if=$ctrl.article.images.url_720 lazy-src={{::$ctrl.article.images.url_720}} animate-visible=true animate-speed=0.5s></a> <a class=post-gz data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><h2 class=post-title data-ng-bind=$ctrl.article.title></h2></a><div submit-required=true ng-bind-html=\"($ctrl.article.resume | limitTo: 400)\"></div><div class=\"col-xs-12 col-md-12 no-sp\"><span class=\"label label-default tag-gz sp\" ng-repeat=\"tag in $ctrl.article.tags track by $index\"><a data-ng-href=#!/{{::tag.label}}>{{::tag.label}}</a></span></div></div></article><article ng-if=\"$ctrl.tipo==2\"><div class=\"col-sm-4 col-md-4 col-xs-4 no-sp\"><div class=dn-sep>“</div></div><div class=\"col-sm-8 col-md-8 col-xs-8 no-sp\"><div class=dn-author style=top:0>Autor: @<a ng-href=#!/{{::$ctrl.article.user.username}}>{{::$ctrl.article.user.username}}</a></div></div><div class=\"col-sm-12 col-md-12 col-xs-12 no-sp\"><div class=\"dn-head-meta dn-date-share\"><p class=no-mg><a data-ng-href=#!/noticias/category/{{::$ctrl.article.category}}>- {{::option[$ctrl.article.category].label}} -</a> <time class=times am-time-ago=$ctrl.article.published_date></time></p></div><div class=dn-share><a class=sicon-tw href=\"https://twitter.com/share?text={{::$ctrl.article.title}};url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'twitter-share', 'width=550,height=235');return false\"></a> <a class=sicon-f href=\"https://www.facebook.com/sharer/sharer.php?u=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'facebook-share','width=580,height=296');return false\"></a> <a class=sicon-g href=\"https://plus.google.com/share?url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'google-plus-share', 'width=490,height=530');return false\"></a></div><a data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><img class=\"titular-gz no-mg\" ng-if=$ctrl.article.images.thumb_380 lazy-src={{::$ctrl.article.images.thumb_380}} animate-visible=true animate-speed=0.5s></a> <a class=post-gz data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><h4 class=post-title data-ng-bind=$ctrl.article.title></h4></a></div></article><article ng-if=\"$ctrl.tipo==3\"><div class=\"col-sm-4 col-md-4 col-xs-4 no-sp\"><div class=dn-sep>“</div></div><div class=\"col-sm-8 col-md-8 col-xs-8 no-sp\"><div class=dn-author>Autor: @<a ng-href=#!/{{::$ctrl.article.user.username}}>{{::$ctrl.article.user.username}}</a></div></div><div class=\"col-sm-12 col-md-12 col-xs-12 no-sp\"><div class=\"dn-head-meta dn-date-share\"><p class=no-mg><a data-ng-href=#!/noticias/category/{{::$ctrl.article.category}}>- {{::option[$ctrl.article.category].label}} -</a> <time class=times am-time-ago=$ctrl.article.published_date></time></p></div><div class=dn-share><a class=sicon-tw href=\"https://twitter.com/share?text={{::$ctrl.article.title}};url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'twitter-share', 'width=550,height=235');return false\"></a> <a class=sicon-f href=\"https://www.facebook.com/sharer/sharer.php?u=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'facebook-share','width=580,height=296');return false\"></a> <a class=sicon-g href=\"https://plus.google.com/share?url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'google-plus-share', 'width=490,height=530');return false\"></a></div><a class=post-gz data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><h1 class=post-title data-ng-bind=$ctrl.article.title></h1></a> <a data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><img class=\"titular-gz no-mg\" ng-if=$ctrl.article.images.url_720 lazy-src={{::$ctrl.article.images.url_720}} animate-visible=true animate-speed=0.5s></a> <a class=post-gz data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><h2 class=post-title data-ng-bind=$ctrl.article.headlines></h2></a><div ng-bind-html=$ctrl.article.content></div><div class=\"col-xs-12 col-md-12 no-sp\"><span class=\"label label-default tag-gz sp\" ng-repeat=\"tag in $ctrl.article.tags track by $index\"><a data-ng-href=#!/{{::tag.label}}>{{::tag.label}}</a></span></div></div></article><article ng-if=\"$ctrl.tipo==4\"><a data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><img class=titular-gz ng-if=$ctrl.article.images.url_1080 lazy-src={{::$ctrl.article.images.url_1080}} animate-visible=true animate-speed=0.5s></a><div class=\"col-sm-4 col-md-4 col-xs-4 no-sp\"></div><div class=\"col-sm-8 col-md-8 col-xs-8 no-sp\"></div><div class=\"col-sm-12 col-md-12 col-xs-12 no-sp\"><div class=\"col-xs-12 col-sm-6 col-md-6 no-sp\"><div class=\"dn-head-meta dn-date-share\"><p class=no-mg><a data-ng-href=#!/noticias/category/{{::$ctrl.article.category}}>- {{::option[$ctrl.article.category].label}} -</a> <time class=times am-time-ago=$ctrl.article.published_date></time></p></div><div class=dn-share><a class=sicon-tw href=\"https://twitter.com/share?text={{::$ctrl.article.title}};url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'twitter-share', 'width=550,height=235');return false\"></a> <a class=sicon-f href=\"https://www.facebook.com/sharer/sharer.php?u=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'facebook-share','width=580,height=296');return false\"></a> <a class=sicon-g href=\"https://plus.google.com/share?url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'google-plus-share', 'width=490,height=530');return false\"></a></div></div><div class=\"col-xs-12 col-sm-6 col-md-6\"><span class=tag-gz-tag><span class=\"label label-default tag-gz sp\" ng-repeat=\"tag in $ctrl.article.tags track by $index\"><a data-ng-href=#!/{{::tag.label}}>{{::tag.label}}</a></span></span></div></div><div class=clearfix></div><a class=post-gz data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><h2 class=post-title>{{::$ctrl.article.title}}</h2><p class=\"col-xs-12 col-md-12 post-content no-sp\"><div submit-required=true ng-bind-html=\"$ctrl.article.resume | limitTo: 400\"></div></p></a></article><article ng-if=\"$ctrl.tipo==5\"><div class=\"col-sm-4 col-md-4 col-xs-4 no-sp\"><div class=dn-sep>“</div></div><div class=\"col-sm-8 col-md-8 col-xs-8 no-sp\"><div class=dn-author>Autor: @<a ng-href=#!/{{::$ctrl.article.user.username}}>{{::$ctrl.article.user.username}}</a></div></div><div class=\"col-sm-12 col-md-12 col-xs-12 no-sp\"><div class=\"dn-head-meta dn-date-share\"><p class=no-mg><a data-ng-href=#!/noticias/category/{{::$ctrl.article.category}}>- {{::option[$ctrl.article.category].label}} -</a> <time class=times am-time-ago=$ctrl.article.published_date></time></p></div><div class=dn-share><a class=sicon-tw href=\"https://twitter.com/share?text={{::$ctrl.article.title}};url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'twitter-share', 'width=550,height=235');return false\"></a> <a class=sicon-f href=\"https://www.facebook.com/sharer/sharer.php?u=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'facebook-share','width=580,height=296');return false\"></a> <a class=sicon-g href=\"https://plus.google.com/share?url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'google-plus-share', 'width=490,height=530');return false\"></a></div><a class=post-gz data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><h2 class=post-title data-ng-bind=$ctrl.article.title style=margin-bottom:25px></h2></a><div class=row ng-if=$ctrl.article.images.thumb_380><div class=\"col-md-4 col-xs-12 no-sp\" data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><img class=\"titular-gz no-mg\" lazy-src={{::$ctrl.article.images.thumb_380}} animate-visible=true animate-speed=0.5s></div><div class=\"col-md-8 col-xs-12\" submit-required=true ng-bind-html=\"($ctrl.article.resume | limitTo: 300)\"></div></div><div class=row ng-if=!$ctrl.article.images.thumb_380><div class=\"col-md-12 col-xs-12\" submit-required=true ng-bind-html=\"($ctrl.article.resume | limitTo: 300)\"></div></div><div class=\"col-xs-12 col-md-12 no-sp\"><span class=\"label label-default tag-gz sp\" ng-repeat=\"tag in $ctrl.article.tags track by $index\"><a data-ng-href=#!/{{::tag.label}}>{{::tag.label}}</a></span></div></div></article>");
}]);

angular.module("templates/category-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/category-client-template.html",
    "<div class=\"col-xs-12 dn-read-gz\" ng-if=\"tipo==1 && idcat != 3\"><h2>{{option[idcat].label}}</h2><div class=news-gz ng-repeat=\"article in catNews | limitTo:5\"><box tipo=2 article=article class=bl ng-show=$first></box><div class=clearfix></div><ul class=read-list ng-show=!$first><li class=clearfix><span class=dn-read-time><span class=date>24</span> <span class=month>Dec</span></span> <a href=/#!/articulo/{{article.slug}}/{{article.published_date}}><span class=dn-read-content><span class=contenido>{{article.title}}</span></span></a></li></ul></div></div><div class=\"col-xs-12 dn-read-pr\" ng-if=\"tipo==2 && idcat != 3\"><h2>{{option[idcat].label}}</h2><div class=news-gz ng-repeat=\"article in catNews | limitTo:5\"><box tipo=1 article=article class=bl ng-show=$first></box><div class=\"col-xs-12 col-md-6\" ng-class=\"{'clear': $index==3}\" ng-show=!$first><box tipo=1 article=article></box></div></div></div><div class=\"col-xs-12 dn-read-gz\" ng-show=\"tipo==1 && idcat == 3\"><h2>Opinión</h2><div class=news-gz><ul class=read-list><li class=\"dn-line clearfix\" ng-repeat=\"article in catNews | limitTo:6\"><div class=\"col-xs-12 col-md-12 no-sp\"><p class=\"dn-head-meta italic\">Por @{{article.user.username}}</p><p class=\"dn-head-meta italic\" style=\"float:right; font-family: serif\">{{article.published_date | date:'dd/MM/yyyy h:mma'}}</p></div><a href=/#!/articulo/{{article.slug}}/{{article.published_date}}><img class=\"col-xs-4 col-md-4 no-sp\" lazy-src=https://res.cloudinary.com/akolor/image/upload/c_crop,h_200,w_200/{{article.user.image}}><span class=\"dn-read-content col-xs-8 col-md-8\"><span class=contenido>{{article.title | limitTo: 66}}...</span></span></a></li></ul></div></div><div class=\"col-xs-12 dn-read-pr\" ng-show=\"tipo==2 && idcat == 3\"><h2>Opinión</h2><div class=news-gz><ul class=read-list><li class=\"dn-line clearfix\" ng-repeat=\"article in catNews | limitTo:6\"><div class=\"col-xs-12 col-md-12 no-sp\"><p class=\"dn-head-meta italic\">Por @{{article.user.username}}</p><p class=\"dn-head-meta italic\" style=\"float:right; font-family: serif\">{{article.published_date | date:'dd/MM/yyyy h:mma'}}</p></div><a href=/#!/articulo/{{article.slug}}/{{article.published_date}}><img class=\"col-xs-2 col-md-2 no-sp\" lazy-src=https://res.cloudinary.com/akolor/image/upload/c_crop,h_200,w_200/{{article.user.image}}><span class=\"dn-read-content col-xs-10 col-md-10\"><span class=\"contenido wd-gz-op\">{{article.title | limitTo: 66}}...</span></span></a></li></ul></div></div>");
}]);

angular.module("templates/chart-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/chart-client-template.html",
    "<div class=\"col-xs-12 dn-read-gz\"><h2>Stock Market</h2><img class=col-xs-12 lazy-src=\"https://chart.finance.yahoo.com/z?s={{$ctrl.title}}&t=1d&q=c&l=on&z=l&p=m5,m200\" style=margin-bottom:10px><div class=col-md-12 ng-repeat=\"n in data1 track by $index\" style=\"font-family:serif; margin-top:10px\" ng-show=n.LastTradePriceOnly><span class=date style=color:black>{{n.Symbol}} : {{n.LastTradePriceOnly}}</span> <span class=title ng-show=\"n.Change<0 && n.Change != undefined\" style=color:red>{{n.Change}} ({{calc(n.LastTradePriceOnly, n.Open)| limitTo:5}}%)</span> <span class=title ng-show=\"n.Change>0 && n.Change != undefined\" style=color:green>{{n.Change}} ({{calc(n.LastTradePriceOnly, n.Open) | limitTo:5}}%)</span> <span class=title ng-show=\"n.Change==0 && n.Change != undefined\" style=color:black>{{n.Change}} ({{calc(n.LastTradePriceOnly, n.Open) | limitTo:5}}%)</span> <span class=date style=color:black>{{n.LastTradeDate | date:'medium'}} {{n.LastTradeTime}}</span></div></div>");
}]);

angular.module("templates/encuesta-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/encuesta-client-template.html",
    "<div class=\"col-xs-12 dn-read-gz\"><div class=news-gz><h2>Encuesta</h2><h4>{{quest.question}}</h4><div class=radio ng-repeat=\"ans in quest.options\" ng-show=!resultView><label><input type=radio name=optionsRadios ng-model=seleccion.indice value={{$index}}> {{ans.text}}</label></div><div class=radio ng-repeat=\"ans in quest.options\" ng-show=resultView><label>{{ans.text}}: {{ans.vote}} votos</label></div><button type=submit class=\"btn btn-gz\" ng-click=vote() ng-show=!voted>Votar</button> <button type=submit class=\"btn btn-gz\" ng-click=resultBtn() ng-show=!voted>Resultados</button></div></div>");
}]);

angular.module("templates/hash-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/hash-client-template.html",
    "<div class=\"col-xs-12 dn-read-gz\"><h2 name=hash>#{{hash}}</h2><div class=news-gz ng-repeat=\"article in hashNews.results\"><box tipo=2 article=article ng-show=$first></box><div class=clearfix></div><ul class=read-list ng-show=!$first><li class=clearfix><span class=dn-read-time><span class=date>24</span> <span class=month>Dec</span></span> <a href=/#!/articulo/{{article.slug}}/{{article.published_date}}><span class=dn-read-content><span class=contenido>{{article.title}}</span></span></a></li></ul></div></div>");
}]);

angular.module("templates/image-day-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/image-day-client-template.html",
    "<div class=\"dn-read-gz col-md-12\"><h2 class=\"col-xs-12 no-sp\">Imagen del Día</h2><div class=\"col-sm-4 col-md-4 col-xs-4 no-sp\"><div class=dn-sep>“</div></div><div class=\"col-sm-8 col-md-8 col-xs-8 no-sp\"><div class=dn-author>Autor: @{{imageNews.user.username}}</div></div><div class=\"col-sm-12 col-md-12 col-xs-12 no-sp\"><div class=\"dn-head-meta dn-date-share\"><time am-time-ago=imageNews.published_date></time></div><div class=dn-share><a class=sicon-tw href=\"https://twitter.com/share?text=Odamae%20-%20free%20contact%20form%20service%20from%20Phantomus&amp;url=http://dayandnight-demo.phantomus.com/odamae-free-contact-form-service-from-phantomus/\" onclick=\"window.open(this.href, 'twitter-share', 'width=550,height=235');return false\"></a> <a class=sicon-f href=\"https://www.facebook.com/sharer/sharer.php?u=http://dayandnight-demo.phantomus.com/odamae-free-contact-form-service-from-phantomus/\" onclick=\"window.open(this.href, 'facebook-share','width=580,height=296');return false\"></a> <a class=sicon-g href=\"https://plus.google.com/share?url=http://dayandnight-demo.phantomus.com/odamae-free-contact-form-service-from-phantomus/\" onclick=\"window.open(this.href, 'google-plus-share', 'width=490,height=530');return false\"></a></div><h3 class=\"col-xs-12 no-sp\">{{imageNews.title}}</h3><img class=\"col-xs-12 no-sp\" lazy-src={{imageNews.images.url_720}}><h4 class=\"col-xs-12 no-sp\">{{imageNews.headlines}}</h4></div></div>");
}]);

angular.module("templates/lasted-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/lasted-client-template.html",
    "<div ng-if=\"(stock && data1) || (!stock && news)\"><affixer class=lasted><div><div class=\"col-xs-1 col-sm-1 dn-title-gz hot-btn\" ng-click=cambio() style=\"cursor: pointer\"><span class=\"glyphicon glyphicon-fire\" aria-hidden=true ng-if=!stock></span> <span class=\"glyphicon glyphicon-usd\" aria-hidden=true ng-if=stock></span></div><div class=\"col-xs-11 col-sm-11 col-md-11 dn-ticker-gz\" ng-class=\"{ 'gblack': stock }\"><div class=dn-ticker-gz-inner ng-if=!stock><div><div id=news_strip ng-mouseover=\"conf.news_move_flag=false\" ng-mouseout=\"conf.news_move_flag=true\"><div class=\"shadow left\">&nbsp;</div><a ng-repeat=\"(k, n) in news track by $index\" ng-style=\"{'right': get_news_right($index)}\" id=news_{{$index}} rel=nofollow target=_blank ng-href=/{{n.slug}}/n.published_date><span class=date>{{n.published_date | date:'h:mma'}}</span> <span class=title style=font-size:14px>{{n.title | limitTo:50}}...</span></a><div class=\"shadow right\">&nbsp;</div></div></div></div><div class=dn-ticker-gz-inner ng-if=stock><div><div id=news_strip ng-mouseover=\"conf.news_move_flag=false\" ng-mouseout=\"conf.news_move_flag=true\"><a ng-repeat=\"n in data1 track by $index\" ng-style=\"{'right': get_news_right($index)}\" id=news_{{$index}} rel=nofollow target=_blank style=width:280px!important><span class=date style=color:#c5c5c5>{{n.Symbol}} : {{n.LastTradePriceOnly}}</span> <span class=title ng-show=\"n.Change<0 && n.Change != undefined\" style=color:red>{{n.Change}} ({{calc(n.LastTradePriceOnly, n.Open)| limitTo:5}}%)</span> <span class=title ng-show=\"n.Change>0 && n.Change != undefined\" style=color:green>{{n.Change}} ({{calc(n.LastTradePriceOnly, n.Open) | limitTo:5}}%)</span> <span class=title ng-show=\"n.Change==0 && n.Change != undefined\" style=color:gray>{{n.Change}} ({{calc(n.LastTradePriceOnly, n.Open) | limitTo:5}}%)</span> <span class=date style=color:#c5c5c5>{{n.LastTradeDate}} - {{n.LastTradeTime}}</span></a></div></div></div></div></div></affixer><div class=statico><div class=\"col-xs-1 col-sm-1 dn-title-gz hot-btn\" ng-click=cambio() style=\"cursor: pointer\"><span class=\"glyphicon glyphicon-fire\" aria-hidden=true ng-if=!stock></span> <span class=\"glyphicon glyphicon-usd\" aria-hidden=true ng-if=stock></span></div><div class=\"col-xs-11 col-sm-11 col-md-11 dn-ticker-gz\" ng-class=\"{'gblack': stock }\"><div class=dn-ticker-gz-inner ng-if=!stock><div><div id=news_strip ng-mouseover=\"conf.news_move_flag=false\" ng-mouseout=\"conf.news_move_flag=true\"><div class=\"shadow left\">&nbsp;</div><a ng-repeat=\"(k, n) in news track by $index\" ng-style=\"{'right': get_news_right($index)}\" id=news_{{$index}} rel=nofollow target=_blank href={{n.id}}><span class=date>{{n.published_date | date:'h:mma'}}</span> <span class=title style=font-size:14px>{{n.title | limitTo:50}}...</span></a><div class=\"shadow right\">&nbsp;</div></div></div></div><div class=dn-ticker-gz-inner ng-if=stock><div><div id=news_strip ng-mouseover=\"conf.news_move_flag=false\" ng-mouseout=\"conf.news_move_flag=true\"><a ng-repeat=\"n in data1 track by $index\" ng-style=\"{'right': get_news_right($index)}\" id=news_{{$index}} rel=nofollow target=_blank style=width:280px!important><span class=date style=color:#c5c5c5>{{n.Symbol}} : {{n.LastTradePriceOnly}}</span> <span class=title ng-show=\"n.Change<0 && n.Change != undefined\" style=color:red>{{n.Change}} ({{calc(n.LastTradePriceOnly, n.Open)| limitTo:5}}%)</span> <span class=title ng-show=\"n.Change>0 && n.Change != undefined\" style=color:green>{{n.Change}} ({{calc(n.LastTradePriceOnly, n.Open) | limitTo:5}}%)</span> <span class=title ng-show=\"n.Change==0 && n.Change != undefined\" style=color:gray>{{n.Change}} ({{calc(n.LastTradePriceOnly, n.Open) | limitTo:5}}%)</span> <span class=date style=color:#c5c5c5>{{n.LastTradeDate}} - {{n.LastTradeTime}}</span></a></div></div></div></div></div></div>");
}]);

angular.module("templates/miniplayer-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/miniplayer-client-template.html",
    "<aside class=mini ng-if=$ctrl.look ng-mouseover=$ctrl.bigg() ng-mouseleave=$ctrl.smal()><span class=btitle>{{$ctrl.title}}</span> <span class=\"bclose glyphicon glyphicon-remove-circle\" ng-click=$ctrl.close() ng-show=$ctrl.big></span> <span class=\"bclose glyphicon glyphicon-volume-up\" ng-click=$ctrl.vol() ng-show=\"$ctrl.mute && $ctrl.big\"></span> <span class=\"bclose glyphicon glyphicon-volume-off\" ng-click=$ctrl.vol() ng-show=\"!$ctrl.mute && $ctrl.big\"></span> <span class=\"bclose glyphicon glyphicon-resize-full\" style=\"font-size: 13px;\n" +
    "    margin-top: 8px; margin-right:8px\" ng-click=$ctrl.totv() ng-show=$ctrl.big></span><youtube auto-play=1 mute=$ctrl.mute video-id=$ctrl.mini width=155 height=87 show-info=0 control=0 disable-kb=1 auto-hide=0 rel=0 modest-branding=1 ng-click=$ctrl.totv()></youtube></aside><aside class=\"mini mi\" ng-if=!$ctrl.look><span class=\"glyphicon glyphicon-expand bopen\" ng-click=$ctrl.close()></span></aside>");
}]);

angular.module("templates/most-read-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/most-read-client-template.html",
    "<div class=\"col-xs-12 dn-read-gz\"><h2>Más Leídos</h2><ul class=read-list><li class=clearfix ng-repeat=\"article in topNews | limitTo:8\"><span class=\"dn-read-time dn-top\"><span class=date>{{$index+1}}</span> <span class=month></span></span> <a href=/#!/articulo/{{article.slug}}/{{article.published_date}}><span class=dn-read-content><span class=contenido>{{article.title}}</span></span></a></li></ul></div>");
}]);

angular.module("templates/newsletter-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/newsletter-client-template.html",
    "<div class=\"col-xs-12 dn-read-gz\"><div class=news-gz><h2>Newsletter</h2><p>Suscríbete ahora y recibe diariamente nuestras noticias</p><div class=input-group><div class=input-group-btn><button type=button class=\"btn btn-gz\">Suscríbete</button></div><input class=\"form-control btn-gz\" placeholder=E-mail aria-describedby=sizing-addon2></div></div></div>");
}]);

angular.module("templates/sidebar-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/sidebar-client-template.html",
    "<div class=\"hidden-xs col-sm-5 col-md-4 sidebar\" style=margin-top:50px ng-show=cargado><div class=\"col-xs-12 col-sm-12 col-md-12 no-sp\" style=\"margin-bottom: -100px\"><mostread></mostread><banner banners=banners_B1></banner><hash hash=::etiqueta></hash><chart url=::link></chart><opinion tipo=1></opinion><videos tipo=1></videos><encuesta></encuesta><category idcat=1 tipo=1></category><twitter></twitter><category idcat=2 tipo=1 style=\"margin-bottom:10px;    float: left;\n" +
    "width: 100%\"></category></div><div class=\"col-xs-12 col-sm-12 col-md-12 no-sp sticky\" stickyfill top=100><div><newsletter></newsletter><banner banners=banners_B1></banner><div style=opacity:0.7><img style=width:100% lazy-src=modules/core/img/gazeta-svg/gz-ccs-home.svg><p style=text-align:center>Copyright © 2016 Aviso legal<br>Todos los derechos reservados</p></div></div></div></div>");
}]);

angular.module("templates/tags.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/tags.html",
    "<div class=\"form-control decipher-tags\" data-ng-mousedown=selectArea()><div class=decipher-tags-taglist><span data-ng-repeat=\"tag in tags|orderBy:orderBy\" data-ng-mousedown=$event.stopPropagation()><span class=decipher-tags-tag data-ng-class=getClasses(tag)>{{tag.label}} <i class=\"glyphicon glyphicon-remove-circle\" data-ng-click=removeTag(tag)></i></span></span></div><span class=wrapper data-ng-show=toggles.inputActive><input ng-if=!srcTags.length data-ng-model=inputTag class=\"decipher-tags-input\"> <input ng-if=srcTags.length data-ng-model=inputTag class=decipher-tags-input data-typeahead=\"stag as stag.label for stag in srcTags|filter:$viewValue|orderBy:orderBy\" data-typeahead-input-formatter={{typeaheadOptions.inputFormatter}} data-typeahead-loading={{typeaheadOptions.loading}} data-typeahead-min-length={{typeaheadOptions.minLength}} data-typeahead-template-url={{typeaheadOptions.templateUrl}} data-typeahead-wait-ms={{typeaheadOptions.waitMs}} data-typeahead-delimiter={{typeaheadOptions.delimiter}} data-typeahead-editable={{typeaheadOptions.allowsEditable}} data-typeahead-on-select=\"add($item) && selectArea() && typeaheadOptions.onSelect()\"></span></div>");
}]);

angular.module("templates/top-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/top-client-template.html",
    "<div class=row ng-controller=top-controller data-ng-init=inicializar() style=\"min-height: 100px\"><a href=\"/#!/\"><img class=\"col-xs-12 col-sm-12 logo-gz-cat no-sp\" ng-click=\"clickChange('home')\" ng-if=category lazy-src=modules/core/img/gazeta-svg/gazeta-ccs-{{category}}.svg style=\"min-height: 80px; margin-bottom:10px\"></a><h5 class=\"date-gz col-xs-12\">Caracas, {{fecha | date:'fullDate'}}</h5><div class=col-xs-12><div class=\"hidden-xs col-sm-12 col-md-9 col-lg-8 centering category-gz\"><a type=button class=\"btn btn-md boton-gz post-gz\" ng-class=\"{active:category=='politica'}\" data-ng-href=#!/noticias/category/0>POLÍTICA</a> <a type=button class=\"btn btn-md boton-gz post-gz\" ng-class=\"{active:category=='economia'}\" data-ng-href=#!/noticias/category/1>ECONOMÍA</a> <a type=button class=\"btn btn-md boton-gz post-gz\" ng-class=\"{active:category=='sucesos'}\" data-ng-href=#!/noticias/category/2>SUCESOS</a> <a type=button class=\"btn btn-md boton-gz post-gz\" ng-class=\"{active:category=='opinion'}\" data-ng-href=#!/noticias/category/3>OPINIÓN</a> <a type=button class=\"btn btn-md boton-gz-last post-gz\" ng-class=\"{active:category=='tv'}\" data-ng-href=#!/noticias/tv>TV</a></div></div><div class=clearfix></div></div>");
}]);

angular.module("templates/twitter-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/twitter-client-template.html",
    "<div class=\"col-xs-12 dn-read-gz\" data-ng-controller=TwitterController><div class=news-gz><br><div class=\"col-xs-11 col-xs-10 col-md-10\"><h4>Twitter</h4></div><div class=\"glyphicon glyphicon-refresh load-circle col-xs-1 col-md-1\" id=loading-activo ng-click=refresh()></div><ul class=no-sp><li ng-repeat=\"tweet in tweets.data | limitTo: 6\" class=\"dn-line clearfix\"><img class=\"col-xs-3 col-md-3\" lazy-src={{tweet.user.profile_image_url}} style=\"top: 10px;padding:0 10px 0 12px\"><div class=\"col-xs-6 col-md-6 no-sp\"><h5>@{{tweet.user.screen_name}}</h5><h5>{{tweet.user.name}}</h5></div><span class=\"col-xs-3 col-md-3 dn-read-time\" style=\"padding:0; margin:5px auto\"><span class=date>{{formatDate(tweet.created_at) | date:'dd'}}</span> <span class=month>{{formatDate(tweet.created_at) | date:'MMM'}}</span></span><div class=clearfix></div><div class=\"col-xs-12 col-md-12\"><small ng-bind-html=\"tweet.text | linky | tweetLinky\"></small><br><small>{{formatDate(tweet.created_at) | date:'medium'}}</small></div></li></ul></div></div>");
}]);

angular.module("templates/uploadFile-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/uploadFile-client-template.html",
    "<div class=row nv-file-drop=\"\" uploader=uploader><h2>Uploads Image</h2><div class=col-md-12 ng-show=\"image_uploaded != undefined\"><div class=col-md-8><img lazy-src={{image_uploaded.thumb_380}} alt=\"\" class=\"img-responsive\"></div><div class=col-md-4><div class=pull-right><button type=button style=margin-bottom:15px class=\"btn btn-danger btn-xs\" ng-click=uploader.queue[0].remove();reset();><span class=\"glyphicon glyphicon-trash\"></span> Remove</button></div></div></div>{{image_uploaded}}<div class=col-md-12 ng-show=\"image_uploaded == undefined\"><h3>Select file</h3><div ng-show=uploader.isHTML5><div class=\"well my-drop-zone\" nv-file-over=\"\" uploader=uploader>Drop File Here</div></div><input type=file nv-file-select=\"\" uploader=\"uploader\"><br></div><div class=col-md-12 style=\"margin-bottom: 40px\"><table class=table><thead><tr><th ng-show=uploader.isHTML5>Size</th><th ng-show=uploader.isHTML5>Progress</th><th>Status</th><th>Actions</th></tr></thead><tbody><tr ng-repeat=\"item in uploader.queue\"><td ng-show=uploader.isHTML5 nowrap>{{ item.file.size/1024/1024|number:2 }} MB</td><td ng-show=uploader.isHTML5><div class=progress style=\"margin-bottom: 0\"><div class=progress-bar role=progressbar ng-style=\"{ 'width': item.progress + '%' }\"></div></div></td></tr></tbody></table></div></div>");
}]);

angular.module("templates/videos-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/videos-client-template.html",
    "<div class=\"col-xs-12 dn-read-gz\" ng-show=\"tipo=='Lateral'\"><img class=\"logo-gz-cat no-sp ng-scope\" lazy-src=modules/core/img/gazeta-svg/gazeta-ccs-tv.svg style=\"max-height: 100px\"><div class=news-gz><ul class=read-list><li class=\"dn-line clearfix\" ng-repeat=\"article in youNews.items | limitTo: 6\"><div ng-show=$first><div class=\"col-xs-12 col-md-12 no-sp\"><p class=\"dn-head-meta italic\" style=\"float:right; font-family: serif\">{{article.snippet.publishedAt | date:'dd/MM/yyyy h:mma'}}</p></div><a ng-click=\"clickVideo(article.snippet.resourceId.videoId, article.snippet.title)\"><img class=\"col-xs-12 col-md-12 no-sp\" lazy-src={{article.snippet.thumbnails.medium.url}}><span class=\"dn-read-content col-xs-12 col-md-12\"><h2 class=contenido>{{article.snippet.title}}</h2></span></a></div><div ng-show=!$first><div class=\"col-xs-12 col-md-12 no-sp\"><p class=\"dn-head-meta italic\" style=\"float:right; font-family: serif\">{{article.snippet.publishedAt | date:'dd/MM/yyyy h:mma'}}</p></div><a ng-click=\"clickVideo(article.snippet.resourceId.videoId, article.snippet.title)\"><img class=\"col-xs-4 col-md-4 no-sp\" lazy-src={{article.snippet.thumbnails.medium.url}}><span class=\"dn-read-content col-xs-8 col-md-8\"><span class=contenido>{{article.snippet.title | limitTo: 66}}...</span></span></a></div></li></ul></div></div><div class=\"col-xs-12 dn-read-pr\" ng-show=\"tipo=='Principal'\"><img class=\"logo-gz-cat no-sp ng-scope\" lazy-src=modules/core/img/gazeta-svg/gazeta-ccs-tv.svg style=\"max-height: 100px\"><div class=news-gz><ul class=read-list><li class=\"dn-line clearfix\" ng-repeat=\"article in youNews.items | limitTo: 4\"><div ng-show=$first><div class=\"col-xs-12 col-md-12 no-sp\"><p class=\"dn-head-meta italic\" style=\"float:right; font-family: serif\">{{article.snippet.publishedAt | date:'dd/MM/yyyy h:mma'}}</p></div><a ng-click=\"clickVideo(article.snippet.resourceId.videoId, article.snippet.title)\"><img class=\"col-xs-12 col-md-12 no-sp\" lazy-src={{article.snippet.thumbnails.medium.url}}><span class=\"dn-read-content col-xs-12 col-md-12\"><h2 class=contenido>{{article.snippet.title}}</h2></span></a></div><div ng-show=!$first><div class=\"col-xs-12 col-md-12 no-sp\"><p class=\"dn-head-meta italic\" style=\"float:right; font-family: serif\">{{article.snippet.publishedAt | date:'dd/MM/yyyy h:mma'}}</p></div><a ng-click=\"clickVideo(article.snippet.resourceId.videoId, article.snippet.title)\"><img class=\"col-xs-4 col-md-4 no-sp\" lazy-src={{article.snippet.thumbnails.medium.url}}><span class=\"dn-read-content col-xs-8 col-md-8\"><span class=\"contenido wd-gz-op\">{{article.snippet.title | limitTo: 66}}...</span></span></a></div></li></ul></div></div>");
}]);

angular.module("templates/widgets-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/widgets-client-template.html",
    "<div ng-repeat=\"element in widgets | filter: {posicion:$ctrl.pos}\" ng-init=compilar(element)></div>");
}]);

angular.module("templates/youtube-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/youtube-client-template.html",
    "<div class=\"embed-responsive embed-responsive-16by9\"><iframe id=player class=embed-responsive-item lazy-src={{url}}></iframe></div>");
}]);

angular.module('core').controller('box-controller', ['$scope' ,
	function($scope) {
		$scope.option = [
	    { label: 'Política', value: 0 },
	    { label: 'Economía', value: 1 },
	    { label: 'Sucesos', value: 2 },
	    { label: 'Opinión', value: 3 },
	    { label: 'Imagen del día', value: 4 }
	  	];
	  }]);

angular.module('core').controller('BusquedaController', ['$scope' ,'$stateParams',
	function($scope, $stateParams) {
		 $scope.queryVal = $stateParams.filtro;
	  }]);

angular.module('core').controller('ByUserController', ['$scope','Reddit', '$http', '$stateParams', '$window','$location',
	function($scope, Reddit, $http, $stateParams, $window, $location) {

		$scope.tagview = false;

		$http.get("user/"+$stateParams.filtro, {ignoreLoadingBar:true}).then( function(res){
			if(res.data == null){ $scope.tagview = true;}else{$scope.profile = res.data;}
		});


		var filter = $stateParams.filtro;
		$scope.idcat = $stateParams.filtro;
		var type = 'tags' ;
		$scope.contenedor = new Reddit(6,type,filter,false);

		// ASYNC DETECTOR DE LAS CARGAS
    $scope.$on ("cfpLoadingBar:loading",function (event, data) {
     	$scope.cargado = false;
        return
		});
    $scope.$on ("cfpLoadingBar:completed",function (event, data) {
     	$scope.cargado = true;
        return
		});


		$scope.$on("$viewContentLoaded",
		function() {
			$scope.cargado = true;
			$window.scrollTo(0,0);
		});

}]);

angular.module('core').directive('autoFocus', ['$timeout' ,
	function($timeout) {

    return {
        restrict: 'AC',
        link: function(_scope, _element) {
            $timeout(function(){
                _element[0].focus();
            }, 0);
        }
    };
}]);

'use strict';

angular.module('core').controller('HeaderController', ['$scope',  'Menus', '$http',"$location", '$aside','$rootScope','$document','$timeout',
	function($scope,  Menus, $http, $location, $aside, $rootScope, $document,$timeout) {

		$scope.isCollapsed = false;
		$scope.searchView = false;

//aside
		$scope.asideState = {
		      open: false
		};

    $scope.openAside = function(position, backdrop) {
      $scope.asideState = {
        open: true,
        position: position
      };

      function postClose() {
        $scope.asideState.open = false;
      }

      $aside.open({
        templateUrl: 'modules/core/views/aside.html',
        placement: position,
        size: 'xs',
				windowClass: 'menumo',
        backdrop: backdrop,
        controller: function($scope, $uibModalInstance) {
          $scope.ok = function(e) {
            $uibModalInstance.close();
            e.stopPropagation();
          };
          $scope.cancel = function(e) {
            $uibModalInstance.dismiss();
            e.stopPropagation();
          };
        }
      }).result.then(postClose, postClose);
		}
//END aside



		$scope.typeaheadOpts = {
		  minLength: 3,
		  waitMs: 500,
		  allowsEditable: true
		};

		$scope.menu = Menus.getMenu('topbar');

		var clickIn = false;

		$document.bind('click', function() {

			$timeout(function () {
				if(!clickIn){

					$scope.searchView = false;
					clickIn = false;
				}else{

					clickIn = false;
				}
			}, 5);

		});

		$scope.inside = function() {
			clickIn = true;
		};


		$scope.onClick = function(val){

			$scope.searchView = false;

			if(val.id){

				$location.path("/articulo/"+val.id);
				$scope.searchView = !$scope.searchView;
			}
			else {

				$location.path("/busqueda/"+val);
				$scope.searchView = !$scope.searchView;

			}
		}

		$scope.changeSearch = function() {
			$scope.searchView = !$scope.searchView;
		};

		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});

		$scope.tags = ["SIMADI","ISIS","Gazeta", "Test", "Noticia"];

		$scope.queryArray = function(val) {

			var tipos = ["title","headlines"];
			var todo = [];
			var print = {}

			angular.forEach(tipos, function(tipo,key){

					todo.push($http.get('articles?count=6&filter['+ tipo +']='+val, {ignoreLoadingBar: true
									}).then(function(response){
										return response.data.results.map(function(items){
												return items;
									 });
								}));
			});

			print = todo[0].then(function(res){ return res});
			return print&&todo[1];


	};






	}]);

'use strict';

angular.module('core').controller('HomeController', ['$scope', '$stateParams', '$location','Articles', 'Reddit','cfpLoadingBar','$rootScope', 'ga', '$http','$document','$window',
	function($scope, $stateParams, $location, Articles , Reddit, cfpLoadingBar, $rootScope, ga, $http,  $document, $window) {

		$scope.etiqueta = {label:"estudiantes"};
		$scope.link = 'chart.finance.yahoo.com/z?s=GOOG&t=1d&q=c&l=on&z=l&p=m5,m200';
		$scope.Player = 'njCDZWTI-xg';

		//$scope.link = 'https://www.google.com/finance/getchart?q=GOOG';


// GOOGLE ANALITICS CARGADOR
  ga('create', 'UA-60745039-1', 'www.gazetadecaracas.com');
  ga('send', 'pageview');

// ASYNC DETECTOR DE LAS CARGAS
  $rootScope.$on ("cfpLoadingBar:loading",function (event, data) {
   	$scope.cargado = false;
      return
	});
  $rootScope.$on ("cfpLoadingBar:completed",function (event, data) {
		 $window.prerenderReady = true;
   	$scope.cargado = true;
      return
	});


$rootScope.$on("$viewContentLoaded",
function() {
	$rootScope.cargado = true;
	$window.scrollTo(0,0);
	if($rootScope.isReady){
		load();
	}
});


	$rootScope.$on('modelReady', function(){
		load();
	});

	function load(){
		$rootScope.banners_P1 = [];
		$rootScope.banners_A1 = [];
		$rootScope.banners_B1 = [];
		$rootScope.banners_B2 = [];
		$rootScope.banners_B3 = [];
		angular.forEach($rootScope.modelo.banners , function(element, i){
			if(element.format.value == 2){
					$rootScope.banners_P1.push(element);
				}
			if(element.format.value == 0){
					$rootScope.banners_A1.push(element);
				}
			if(element.format.value == 1){
					$rootScope.banners_B1.push(element);
				}
			if(element.format.value == 3){
					$rootScope.banners_B2.push(element);
				}
			if(element.format.value == 4){
					$rootScope.banners_B3.push(element);
				}
		});

		if($stateParams.tipo == 'category'){
			$scope.articles = $rootScope.modelo[$rootScope.option[$stateParams.filtro].label];
			var lastKey = JSON.stringify($rootScope.modelo[$rootScope.option[$stateParams.filtro].label].lastKey);
			var filter = $stateParams.filtro;
			$scope.idcat = $stateParams.filtro;
			var type = $stateParams.tipo ;
			$scope.contenedor = new Reddit(6,type,filter,false,lastKey);
		}else{
			$scope.articles = $rootScope.modelo.articles;
			var lastKey = JSON.stringify($rootScope.modelo.articles.lastKey);
			$scope.reddit = new Reddit(6,undefined,undefined,false, lastKey);
		}
	}

		// PETICION PARA ARTICULO INDIVIDUAL
		$scope.findOne = function() {
		$scope.readyDisq = false;
		Articles.get({
							articleId: $stateParams.articuloId,
							articleDate: $stateParams.articuloDate
						}, function(response){
							var disqID = 'gazetaccs';
							var url = "https://www.gazetadecaracas.com/#!/articulo/"+response.slug+"/"+response.created_date;
							$scope.article = response;
							$scope.disqusConfig = {
									disqus_shortname: disqID,
									disqus_identifier: response._id,
									disqus_title: response.title,
									disqus_url: url
							};
						});
		};

}

]);

angular.module('core').controller('TagsController', ['$scope', '$http',
	function($scope, $http) {


var url = "tags/"+$stateParams.tags;

$http.get(url,{cache: true}).then(function(data) {
	  $scope.hashNews = data;
});


}]);

angular.module('core').controller('top-controller', ['$scope' , '$stateParams','$rootScope','$location',
	function($scope, $stateParams, $rootScope, $location) {


		$scope.fecha = new Date();

		$scope.activo = {
			0:"",
			1:"",
			2:"",
			3:"",
			4:""
		};



$scope.$on('$stateChangeSuccess', function() {

		if($stateParams.tipo == "category"){


		if ($stateParams.filtro == 0){

			$scope.category = "politica";


		}
		else if ($stateParams.filtro == 1){
			$scope.category = "economia";

		}
		else if ($stateParams.filtro == 2){
			$scope.category = "sucesos";

		}
		else if ($stateParams.filtro == 3){
			$scope.category = "opinion";

		}

		}
		else if ($stateParams.tipo == 'tv'){

			$scope.category = "tv";

		}
		else if ($stateParams.tipo == "tags"){
			$scope.category = "tags";

		}
		else if ($location.path().split("/")[1]== "busqueda"){
			$scope.category = "busqueda";

		}

		else {

			$scope.category = "home";

		 }

	});







}]);

angular.module('core').controller('TvController', ['$scope', '$http', '$location', '$uibModal','$rootScope', '$timeout', '$window',
	function($scope, $http, $location, $uibModal, $rootScope, $timeout, $window) {

	var activo = true;
	var origin = "https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/videos/"
	var playlist1 = origin+"PLFh862iGfy0kkjcTnVDRs1mw__94WARq5";
	var playlist2 = origin+"PLlTLHnxSVuIz6HF8xMupeHpOtTqbyDLW0";
	var playlist3 = origin+"PLlTLHnxSVuIyn2-PlKO6P0FkWZqb0uczg";
	var playlist4 = origin+"PLlTLHnxSVuIyw5jPrLmewrpBJAPYKhgml";
	var playlist5 = origin+"PLlTLHnxSVuIzxAaEd8QDHerZDyJ-AK2Yr";
	var playlist6 = origin+"PLlTLHnxSVuIy0oTb_bFBJL6rfUazkb3y7";
	var playlist7 = origin+"PLlTLHnxSVuIxpJYB7uxXS9PZhHNwTEzOA";
	var playlist8 = origin+"PLlTLHnxSVuIyn2-PlKO6P0FkWZqb0uczg";

	$scope.Player = 'njCDZWTI-xg';
	$scope.auto = 1;

	angular.element(document.getElementsByClassName( 'contenedor' )).bind('scroll', function(){
		$rootScope.$broadcast('tv');
	});

	$scope.initVideos = function(){

		$http.get(playlist1,{ignoreLoadingBar: true, cache:true}).then(
			function(data){
				$rootScope.videosTV1 = data;
			});

		$http.get(playlist2,{ignoreLoadingBar: true, cache:true}).then(function(data){ console.log(data);$scope.videosTV2 = data});
		$http.get(playlist3,{ignoreLoadingBar: true, cache:true}).then(function(data){$scope.videosTV3 = data});
		$http.get(playlist4,{ignoreLoadingBar: true, cache:true}).then(function(data){$scope.videosTV4 = data});
		$http.get(playlist5,{ignoreLoadingBar: true, cache:true}).then(function(data){$scope.videosTV5 = data});
		$http.get(playlist6,{ignoreLoadingBar: true, cache:true}).then(function(data){$scope.videosTV6 = data});
		$http.get(playlist7,{ignoreLoadingBar: true, cache:true}).then(function(data){$scope.videosTV7 = data});
		$http.get(playlist8,{ignoreLoadingBar: true, cache:true}).then(function(data){$scope.videosTV8 = data});

	}


	$scope.clickVideo = function(id , title){

		$rootScope.playVideo = id;
		$rootScope.videoTitle = title;

		var modalInstance = $uibModal.open({
			// Put a link to your template here or whatever
			templateUrl: 'modules/core/views/player.html',
			windowClass: 'tv',
		});
	}

	$scope.clickPic = function(){
      		$scope.activo = !$scope.activo;
	}


}]);

angular.module('core').controller('TwitterController', ['$scope', 'tweets',
	function($scope, tweets) {

			tweets.then(function(res){
				$scope.tweets = res;
			}
		);

		$scope.formatDate = function(fecha){
				var modificado  = new Date(fecha.replace(/-/g,"/"));
				return modificado;
		}

}]);

'use strict';
angular.module('core').component('affixer', {

  controller: ["$scope", "$element", "$document", "$window", "$animate", function ($scope, $element, $document, $window, $animate) {

      var win = angular.element($window);
      var agregado = false;
      affixElement();

      function showClass(){

        $animate.addClass($element[0],'fticker');
        agregado = true;
         //$scope.$digest();
      };

      function hideClass(){

        $animate.removeClass($element[0],'fticker');
        agregado = false;
         //$scope.$digest();
      };

      function affixElement() {

          if ($window.pageYOffset > 800 && !agregado) {
              showClass();
          } else if ($window.pageYOffset < 800 && agregado) {
              hideClass()
          }
      };

      $scope.$on('$routeChangeStart', function() {
          win.unbind('scroll', affixElement);
      });
      win.bind('scroll', affixElement);
  }]
})

angular.module('core').directive('stickyfill', [function($scope) {
  return {
    restrict: 'AEC',
    scope: {
        top: "<",
      },
    link: function(scope, element, window) {

      var tope = scope.top || 50;

      element.css({
        top: tope+'px'
      });
      Stickyfill.add(element[0]);
    }
  };
}]);

'use strict';
angular.module('core').component('banner', {

  bindings: {
    banners: "<"
  },

  templateUrl: "templates/banner-client-template.html",
  controller: ["$scope", function ($scope) {

      var ram = Number;

    this.$onChanges = function (changesObj) {
      if (changesObj.banners) {
        if(changesObj.banners.currentValue != undefined){
          this.banners = changesObj.banners.currentValue;
          ram = (Math.floor(this.banners.length * Math.random() + 1))-1;
          $scope.banner = this.banners[ram];
        }
      }
    };


  }]
});

angular.module('core').component('box', {
  bindings: {
    article: "<",
    tipo: "<"
  },
  templateUrl: 'templates/box-client-template.html',
  controller: 'box-controller',
  controllerAs: '$ctrl',
  bindToController: true
});

angular.module('core').component('category',  {

	bindings: {
  		idcat: "<",
  		tipo: "<"

  	},
    templateUrl: "templates/category-client-template.html",
    controller:["$rootScope", "$scope", function($rootScope, $scope){

      var categoria = $rootScope.option[this.idcat].label;


      //Tipo (1. Cascada 2. Sidebar)
      if(!this.tipo)
      {$scope.tipo=1;}

      $rootScope.$on('modelReady', function(){
          load();
      });

      if($rootScope.isReady){
        load();
      }

      function load(){
        $scope.catNews = $rootScope.modelo[categoria].results;
      }

    }]

});

angular.module('core').component('stock',{

  	bindings: {
  		symbols: "<",
  		tipo: "@",
      title:"@"
  	},
    templateUrl: "templates/chart-client-template.html",
    controller: ["$scope", "Stock", function ($scope, Stock) {


      Stock.start(this.symbols);

      $scope.$on('skUpdate', function(info, payload){
      	$scope.data1 = payload.data.query.results.quote;
      });
      $scope.$on('$destroy', function(info, payload){
      	Stock.stop();
      });

      $scope.calc = function(last, open){
      	var change = open -last;
      	return (change*100)/open;
      }
    }]
});

angular.module('core').directive('dirDisqus', ['$window', function ($window) {
        return {
            restrict: 'E',
            scope: {
                config: '='
            },
            template: '<div id="disqus_thread"></div><a href="https://disqus.com" class="dsq-brlink"></a>',
            link: function (scope) {

                scope.$watch('config', configChanged, true);

                function configChanged() {
                    // Ensure that the disqus_identifier and disqus_url are both set, otherwise we will run in to identifier conflicts when using URLs with "#" in them
                    // see http://help.disqus.com/customer/portal/articles/662547-why-are-the-same-comments-showing-up-on-multiple-pages-
                    if (!scope.config.disqus_shortname ||
                        !scope.config.disqus_identifier ||
                        !scope.config.disqus_url) {
                        return;
                    }


                    $window.disqus_shortname = scope.config.disqus_shortname;
                    $window.disqus_identifier = scope.config.disqus_identifier;
                    $window.disqus_url = scope.config.disqus_url;
                    $window.disqus_title = scope.config.disqus_title;
                    $window.disqus_category_id = scope.config.disqus_category_id;
                    $window.disqus_disable_mobile = scope.config.disqus_disable_mobile;
                    $window.disqus_config = function () {
                        this.language = scope.config.disqus_config_language;
                      //  this.page.remote_auth_s3 = scope.config.disqus_remote_auth_s3;
                        this.page.api_key = scope.config.disqus_api_key;
                        if (scope.config.disqus_on_ready) {
                            this.callbacks.onReady = [function () {
                                scope.config.disqus_on_ready();
                            }];
                        }
                    };

                    // Get the remote Disqus script and insert it into the DOM, but only if it not already loaded (as that will cause warnings)
                    if (!$window.DISQUS) {
                        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                        dsq.src = '//' + scope.config.disqus_shortname + '.disqus.com/embed.js';
                        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                    } else {
                        $window.DISQUS.reset({
                            reload: true,
                            config: function () {
                                this.page.identifier = scope.config.disqus_identifier;
                                this.page.url = scope.config.disqus_url;
                                this.page.title = scope.config.disqus_title;
                                this.language = scope.config.disqus_config_language;
                                //this.page.remote_auth_s3 = scope.config.disqus_remote_auth_s3;
                                this.page.api_key = scope.config.disqus_api_key;
                            }
                        });
                    }
                }
            }
        };
    }]);

angular.module('core').component('custom',  {
	bindings: {
  	 title: "@"
  	},
    templateUrl: "templates/hash-client-template.html",
    controller: ["$scope", "$rootScope", function($scope, $rootScope){

      $scope.hash = this.title;


      $rootScope.$on('modelReady', function(){
            load();
      });

      if($rootScope.isReady){
        load();
      }

      function load(){
				for (var i = 0; i < $rootScope.modelo.widgets.custom.length; i++) {
					if($rootScope.modelo.widgets.custom[i].title == $scope.hash){
						$scope.hashNews = $rootScope.modelo.widgets.custom[i];
						break;
					}
				}
      }


    }]

});

angular.module('core').component('poll',  {

    bindings:{
      id:"@"
    },
    templateUrl: "templates/encuesta-client-template.html",
    controller:["$scope", "$http", "$location", "Vote", "$rootScope", function($scope, $http, $location , Vote, $rootScope) {

    var idn = this.id;

    $scope.seleccion = {indice:0};


      $rootScope.$on('modelReady', function(){
          load();
      });

      if($rootScope.isReady){
        load();
      }

      function load(){

  				for (var i = 0; i < $rootScope.modelo.widgets.poll.length; i++) {
  						if($rootScope.modelo.widgets.poll[i].title == idn){
  							$scope.quest = $rootScope.modelo.widgets.poll[i].results;
  							break;
  						}
  				}

          if($scope.quest){
    				$http.get('https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/ip?_id='+$scope.quest._id, {cache: true})
    					.then(function(res) {

    					if(res.data.message == 'exist'){
    						$scope.resultView = true;
    						$scope.voted = true;
    					}else if(res.data.message == 'available'){
    						$scope.resultView = false;
    						$scope.voted = false;
    					}
    				});
          }
      }

  		$scope.vote = function() {
  			var vote = new Vote ({
  				pollId: $scope.quest._id,  textPos:$scope.seleccion.indice
  			});

  			vote.$save({
  				pollId: $scope.quest._id,  textPos:$scope.seleccion.indice
  			},function(error, res) {
            if(res.data.message === 'success'){
              $scope.quest.options[$scope.seleccion.indice].vote++;
              $scope.resultView = true;
              $scope.voted = true;
            }else{
              console.error('culo');
            }
  			});
  		};

  		$scope.resultBtn = function() {
  			$scope.resultView = !$scope.resultView;
  		};

  }]
});

angular.module('core').component('hashtag',  {
	bindings: {
  	 hash: "@"
  	},
    templateUrl: "templates/hash-client-template.html",
    controller: ["$scope", "$rootScope", function($scope, $rootScope){
      $scope.hash = this.hash;

			$rootScope.$on('modelReady', function(){
						load();
			});

			if($rootScope.isReady){
				load();
			}

			function load(){
				for (var i = 0; i <$rootScope.modelo.widgets.hashtag.length; i++) {
						 if($rootScope.modelo.widgets.hashtag[i].title == $scope.hash){
							 $scope.hashNews = $rootScope.modelo.widgets.hashtag[i].results;
							 break;
						 }
				 }
			}
			
    }]

});

angular.module('core').component('imageday',  {
    templateUrl: "templates/image-day-client-template.html",
    controller: ["$rootScope", "$scope", function($rootScope, $scope){
                $rootScope.$on('modelReady', function(){
                      load();
                });

                if($rootScope.isReady){
                  load();
                }

                function load(){
                  $scope.imageNews = $rootScope.modelo.Imagen.results[0];
                }
    }]

});

angular.module('core').component('lasted', {
  bindings: {
    articles: '<'
  },
  templateUrl: 'templates/lasted-client-template.html',
  controller: ["$scope", "$interval", "$stateParams", "$http", "$rootScope", "Stock", function ($scope, $interval,  $stateParams, $http, $rootScope, Stock) {

          $scope.news = [];

          if($stateParams.filtro==1)
          {
              $scope.stock=true;
          }else{
              $scope.stock=false;
          }

          $scope.conf = {
              news_length: false,
              news_pos: 200, // the starting position from the right in the news container
              news_margin: 20,
              news_move_flag: true
          };


          $scope.cambio = function(){
              $scope.stock=!$scope.stock;
          }

          var ticker =undefined;
          var reloader = undefined;


          $rootScope.$on('modelReady', function(){
                load();
          });

          $scope.$on('skUpdate', function(info, payload){
            $scope.data1 = payload.data.query.results.quote;
            if($scope.news){
              start();
            }
          });

          if($rootScope.isReady){
            load();
          }

          function load(){
            angular.fromJson($rootScope.modelo['articles'].results);
            $scope.news = angular.fromJson(angular.toJson($rootScope.modelo['articles'].results));
            // starting the interval by default
            if($scope.data1){
              start();
            }
          }


          $scope.calc = function(last, open){
            var change = open -last;
            return (change*100)/open;
          }

          function start(){
            stop();
            ticker = $interval($scope.news_move ,50);
          };

          // stops the interval
          function stop() {
              $interval.cancel(ticker);
              Stock.stop();
          };


          $scope.$on('$destroy', function() {
             stop();
           });


        $scope.get_news_right = function(idx) {
            var $right = $scope.conf.news_pos;
            for (var ri=0; ri < idx; ri++) {
                if (document.getElementById('news_'+ri)) {
                    $right += $scope.conf.news_margin + angular.element(document.getElementById('news_'+ri))[0].offsetWidth;
                }
            }
            return $right+'px';
        };

        $scope.news_move = function() {
            if (angular.element(document.getElementById('news_0'))[0]){
              if ($scope.conf.news_move_flag) {
                $scope.conf.news_pos--;
             if ( angular.element(document.getElementById('news_0'))[0].offsetLeft > angular.element(document.getElementById('news_strip'))[0].offsetWidth + $scope.conf.news_margin ) {
                   var first_new = $scope.news[0];
                    $scope.news.pop();
                    $scope.news.unshift(first_new);
                    $scope.conf.news_pos += angular.element(document.getElementById('news_0'))[0].offsetWidth + $scope.conf.news_margin;
                }
            }
          }
        };

  }]
});

angular.module('core')
.directive('lazySrc', ['$window', '$document', '$rootScope', function($window, $document, $rootScope){
    var doc = $document[0],
        body = doc.body,
        win = $window,
        $win = angular.element(win),
        uid = 0,
        elements = {};

    function getUid(el){
        var __uid = el.data("__uid");
        if (! __uid) {
            el.data("__uid", (__uid = '' + ++uid));
        }
        return __uid;
    }

    function getWindowOffset(){
        var t,
            pageXOffset = (typeof win.pageXOffset == 'number') ? win.pageXOffset : (((t = doc.documentElement) || (t = body.parentNode)) && typeof t.ScrollLeft == 'number' ? t : body).ScrollLeft,
            pageYOffset = (typeof win.pageYOffset == 'number') ? win.pageYOffset : (((t = doc.documentElement) || (t = body.parentNode)) && typeof t.ScrollTop == 'number' ? t : body).ScrollTop;
        return {
            offsetX: pageXOffset,
            offsetY: pageYOffset
        };
    }

    function isVisible(iElement){
        var elem = iElement[0],
            elemRect = elem.getBoundingClientRect(),
            windowOffset = getWindowOffset(),
            winOffsetX = windowOffset.offsetX,
            winOffsetY = windowOffset.offsetY,
            elemWidth = elemRect.width,
            elemHeight = elemRect.height,
            elemOffsetX = elemRect.left + winOffsetX,
            elemOffsetY = elemRect.top + winOffsetY,
            viewWidth = Math.max(doc.documentElement.clientWidth, win.innerWidth || 0),
            viewHeight = Math.max(doc.documentElement.clientHeight, win.innerHeight || 0),
            xVisible,
            yVisible;

        if(elemOffsetY <= winOffsetY){
            if(elemOffsetY + elemHeight >= winOffsetY){
                yVisible = true;
            }
        }else if(elemOffsetY >= winOffsetY){
            if(elemOffsetY <= winOffsetY + viewHeight){
                yVisible = true;
            }
        }

        if(elemOffsetX <= winOffsetX){
            if(elemOffsetX + elemWidth >= winOffsetX){
                xVisible = true;
            }
        }else if(elemOffsetX >= winOffsetX){
            if(elemOffsetX <= winOffsetX + viewWidth){
                xVisible = true;
            }
        }

        return xVisible && yVisible;
    };

    function checkImage(){
        Object.keys(elements).forEach(function(key){
            var obj = elements[key],
                iElement = obj.iElement,
                $scope = obj.$scope;
            if(isVisible(iElement)){
                iElement.attr('src', $scope.lazySrc);
            }
        });
    }

    $win.bind('scroll', checkImage);
    $win.bind('resize', checkImage);
    checkImage();
    $rootScope.$on('tv',checkImage);

    function onLoad(){
        var $el = angular.element(this),
            uid = getUid($el);

        $el.css('opacity', 1);

        if(elements.hasOwnProperty(uid)){
            delete elements[uid];
        }
    }

    return {
        restrict: 'A',
        scope: {
            lazySrc: '@',
            animateVisible: '@',
            animateSpeed: '@'
        },
        link: function($scope, iElement){

            iElement.bind('load', onLoad);

            $scope.$watch('lazySrc', function(){
                var speed = "1s";
                if ($scope.animateSpeed != null) {
                    speed = $scope.animateSpeed;
                }
                if(isVisible(iElement)){
                    if ($scope.animateVisible) {
                        iElement.css({
                           //'background-color': '#d5d5d5',
                            'opacity': 0,
                            '-webkit-transition': 'opacity ' + speed,
                            'transition': 'opacity ' + speed
                        });
                    }
                    iElement.attr('src', $scope.lazySrc);
                }else{
                    var uid = getUid(iElement);
                    iElement.css({
                        //'background-color': '#d5d5d5',
                        'opacity': 0,
                        '-webkit-transition': 'opacity ' + speed,
                        'transition': 'opacity ' + speed
                    });
                    elements[uid] = {
                        iElement: iElement,
                        $scope: $scope
                    };
                }
            });

            $scope.$on('$destroy', function(){
                iElement.unbind('load');
                var uid = getUid(iElement);
                if(elements.hasOwnProperty(uid)){
                    delete elements[uid];
                }
            });
        }
    };
}]);

angular.module('core').component('likes',  {

    templateUrl: "templates/most-read-client-template.html",
    controller: ["$scope", "$rootScope", function($scope, $rootScope){
  
      $rootScope.$on('modelReady', function(){
            load();
      });

      if($rootScope.isReady){
        load();
      }

      function load(){
        $scope.topNews = $rootScope.modelo.widgets.likes[0].results;
      }
    }]

});

'use strict';
angular.module('core').component('miniplayer', {
  bindings:{
    title:'@',
    mini:'<',
    look:'@'
  },
  controllerAs: '$ctrl',
  bindToController: true,
  templateUrl: "templates/miniplayer-client-template.html",
  controller:["$animate", "$element", "$scope", "$location", function($animate, $element, $scope, $location){

    this.big = false;
    this.mute = true;

    this.vol = function(){
      this.mute = !this.mute;
    }

    this.totv = function(){
      $location.path('noticias/tv');
    }

    this.bigg = function(){
      this.big = true;
      $animate.addClass($element.children()[0],'bigg');

    };

   this.smal =  function(){
      this.big = false;
      $animate.removeClass($element.children()[0],'bigg');

    };

    this.look = false;

    this.close = function(){
      this.look = !this.look;
    }
  }]
});

angular.module('core').component('popular',  {

    templateUrl: "templates/most-read-client-template.html",
    controller: ["$scope", "$rootScope", function($scope, $rootScope){
      $rootScope.$on('modelReady', function(){
            load();
      });

      if($rootScope.isReady){
        load();
      }

      function load(){
        $scope.topNews = $rootScope.modelo.widgets.popular[0].results;
      }
    }]

});

angular.module('core').directive('newsletter', function() {
  return {
  	//RESTRICCION E para elemento y con A Atributo
  	restrict: "E",


    templateUrl: "templates/newsletter-client-template.html"
  }
});

'use strict';
angular.module('core').component('sidebar', {
  templateUrl: "templates/sidebar-client-template.html"
});

angular.module('core').component('tick', {
  bindings: {
    articles: '<'
  },
  template: "<canvas class='img-responsive col-md-12'></canvas>",
  controller: ["$scope", "$element", "$timeout", "stockService", "$stateParams", function ($scope, $element, $timeout, stockService, $stateParams) {

      	$scope.$watch('$element', function() {
            var symbol=['AAPL','GOOG','MELI','BFR','LND','BAK','BRFS','CBD','EDN','%5EDJI','%5EGSPC'];
            var canvas = $element[0].firstChild;
            angular.element(canvas).attr('height','31');
            angular.element(canvas).attr('width', $element[0].offsetParent.clientWidth);
            //console.log(canvasAll[0].offsetParent.clientHeight);
            box1=canvas.getContext("2d");
            box2=canvas.getContext("2d");
            stockService.value(symbol,box1,canvas);
        });
    }]

  });

angular.module('core').directive('top', function() {
  return {
  	//RESTRICCION E para elemento y con A Atributo
  	restrict: "E",
  	scope: {
  		title: "<"

  	},

    templateUrl: "templates/top-client-template.html"
  }
});

angular.module('core').directive('twitter', function() {
  return {
  	//RESTRICCION E para elemento y con A Atributo
  	restrict: "E",


    templateUrl: "templates/twitter-client-template.html"
  }

});


angular.module('core').filter('tweetLinky',['$filter',
    function($filter) {
        return function(text, target) {
            if (!text) return text;

            var replacedText = $filter('linky')(text, target);

            var targetAttr = "";
            if (angular.isDefined(target)) {
                targetAttr = ' target="' + target + '"';
            }

            // replace #hashtags and send them to twitter
            var replacePattern1 = /(^|\s)#(\w*[a-zA-Z_]+\w*)/gim;
            replacedText = text.replace(replacePattern1, '$1<a href="https://twitter.com/search?q=%23$2"' + targetAttr + '>#$2</a>');

            // replace @mentions but keep them to our site
            var replacePattern2 = /(^|\s)\@(\w*[a-zA-Z_]+\w*)/gim;
            replacedText = replacedText.replace(replacePattern2, '$1<a href="https://twitter.com/$2"' + targetAttr + '>@$2</a>');

            return replacedText;
        };

    }
]);

angular.module('core').directive('uiScrollfix', ['$window', function ($window) {
  'use strict';

  function getWindowScrollTop() {
    if (angular.isDefined($window.pageYOffset)) {
      return $window.pageYOffset;
    } else {
      var iebody = (document.compatMode && document.compatMode !== 'BackCompat') ? document.documentElement : document.body;
      return iebody.scrollTop;
    }
  }
  return {
    require: '^?uiScrollfixTarget',
    link: function (scope, elm, attrs, uiScrollfixTarget) {
      var absolute = true,
          shift = 0,
          fixLimit,
          $target = uiScrollfixTarget && uiScrollfixTarget.$element || angular.element($window);

      if (!attrs.uiScrollfix) {
          absolute = false;
      } else if (typeof(attrs.uiScrollfix) === 'string') {
        // charAt is generally faster than indexOf: http://jsperf.com/indexof-vs-charat
        if (attrs.uiScrollfix.charAt(0) === '-') {
          absolute = false;
          shift = - parseFloat(attrs.uiScrollfix.substr(1));
        } else if (attrs.uiScrollfix.charAt(0) === '+') {
          absolute = false;
          shift = parseFloat(attrs.uiScrollfix.substr(1));
        }
      }

      fixLimit = absolute ? attrs.uiScrollfix : elm[0].offsetTop + shift;

      function onScroll() {

        var limit = absolute ? attrs.uiScrollfix : elm[0].offsetTop + shift;

        // if pageYOffset is defined use it, otherwise use other crap for IE
        var offset = uiScrollfixTarget ? $target[0].scrollTop : getWindowScrollTop();
        if (!elm.hasClass('ui-scrollfix') && offset > limit) {
          elm.addClass('ui-scrollfix');
          fixLimit = limit;
        } else if (elm.hasClass('ui-scrollfix') && offset < fixLimit) {
          elm.removeClass('ui-scrollfix');
        }
      }

      $target.on('scroll', onScroll);

      // Unbind scroll event handler when directive is removed
      scope.$on('$destroy', function() {
        $target.off('scroll', onScroll);
      });
    }
  };
}]).directive('uiScrollfixTarget', [function () {
  'use strict';
  return {
    controller: ['$element', function($element) {
      this.$element = $element;
    }]
  };
}]);

angular.module('core').component('user',  {
	bindings: {
  	 username: "@"
  	},
    templateUrl: "templates/hash-client-template.html",
    controller: ["$scope", "$rootScope", function($scope, $rootScope){
      $scope.hash = this.username;

      $rootScope.$on('modelReady', function(){
        load();
      });

			if($rootScope.isReady){
				load();
			}

			function load(){
				for (var i = 0; i < $rootScope.modelo.widgets.user.length; i++) {
          if($rootScope.modelo.widgets.user[i].title == $scope.hash){
            $scope.hashNews = $rootScope.modelo.widgets.user[i].results;
            break;
          }
        }
			}

    }]

});

angular.module('core').component('tv',  {
	bindings: {
  	 title: "@",
     tipo: "@"
  	},
    templateUrl: "templates/videos-client-template.html",
    controller: ["$scope", "$uibModal", "$rootScope", "$document", function($scope, $uibModal, $rootScope, $document) {

    var title = this.title;
    $scope.tipo = this.tipo;

    $rootScope.$on('modelReady', function(){
			load();
    });

		if($rootScope.isReady){
			load();
		}

		function load(){
			for (var i = 0; i < $rootScope.modelo.widgets.tv.length; i++) {
				if($rootScope.modelo.widgets.tv[i].title == title){
					$scope.youNews = $rootScope.modelo.widgets.tv[i].results;
					break;
				}
			}
		}

    $scope.clickVideo = function(id , title){
    	$rootScope.playVideo = id;
    	$rootScope.videoTitle = title;

    	var modalInstance = $uibModal.open({
    		// Put a link to your template here or whatever
    		templateUrl: 'modules/core/views/player.html',
				windowClass: 'tv'

    	});
    }

    }]

});

'use strict';
angular.module('core').component('widget', {
  bindings: {
    section: '@',
    pos: '@'
  },
  bindToController: true,
  controllerAs: '$ctrl',
  templateUrl:"templates/widgets-client-template.html",
  controller: ["$scope", "$element", "$compile", "$http", function ($scope, $element, $compile, $http) {
    var where = this.section;
    var url = "http://gazeta-ccs-files.s3-website-us-west-2.amazonaws.com/config.json";
    $http.get(url).then(function(response, err){
      angular.forEach(response.data, function(element){

          if(element.label == where){
            $scope.widgets = element.widget;
          }
      });
    });

    var pos = this.pos;
    $scope.compilar = function(element){
      var add =""
      if(element.type == 'category'){
        add = "idcat='"+element.category.value+"'";
      }
      if(element.type == 'hashtag'){
        add = "hash='"+element.input+"'";

      }

      if(element.type == 'tv'){
        add = "title='"+element.input+"' tipo='"+pos+"'";
      }
      
      if(element.type == 'user'){
        add = "username='"+element.input+"'";

      }
      if(element.type == 'custom'){
        add = "title='"+element.input+"'";
      }
      if(element.type == 'poll'){
        add = "id='"+element.input+"'";
      }
      if(element.type == 'stock'){
        var arr = '';
        angular.forEach(element.arrayElement, function(elm, key){
          arr = arr + '"' + elm +'"';
          if(element.arrayElement.length - 1 > key){
            arr = arr + ',';
          }
        });
        add = "symbols='["+arr+"]' tipo='"+pos+"' title='"+element.input+"'";
      }
      if(element.type == 'banner'){
        if(element.posicion == 'Lateral'){
          add = "banners='banners_B1'";
        }else{
          add = "banners='banners_P1'";
        }
      }

      var wid = angular.element('<'+element.type+' '+add+'></'+element.type+'>');
      $element.append(wid);
      $compile(wid)($scope);
    }


  }]
});

angular.module('core').directive('opinion', function() {
  return {
  	//RESTRICCION E para elemento y con A Atributo
  	restrict: "E",
	scope: {
  		tipo: "<"

  	},

    templateUrl: "templates/opinion-client-template.html"
  }
});

angular.module('core').directive('youtube', ['$window', 'youtubeApiLoader', '$animate' ,function($window, youtubeApiLoader, $animate) {
  return {
    restrict: "E",

    scope: {
      stateChange: "&",
      playerReady: "&",
      playbackQualityChange: "&",
      playbackRateChange: "&",
      error: "&",
      apiChange: "&",
      videoId: "<", // required
      width: "@",
      height: "@",
      autoHide: "@",
      autoPlay: "@",
      ccLoadPolicy: "@",
      color: "@",
      control: "@",
      disableKb: "@",
      enableJsApi: "@",
      end: "@",
      fs: "@",
      hl: "@",
      ivLoadPolicy: "@",
      list: "@",
      listType: "@",
      loop: "@",
      modestBranding: "@",
      origin: "@",
      playerApiId: "@",
      playlist: "@",
      playsInline: "@",
      rel: "@",
      showInfo: "@",
      start: "@",
      theme: "@",
      mute: "="
    },

    template: '<div></div>',

    link: function(scope, element, attrs) {

    console.log(scope.videoId);

    $animate.addClass(element[0],'embed-responsive embed-responsive-16by9');
    $animate.addClass(element.children()[0],'embed-responsive-item');


      if(!scope.width && !scope.height){
        scope.width = 640;
        scope.height = 480;
      }

      scope.$watch('mute',function() {
        if(scope.mute != undefined){
          if(scope.mute && scope.player != undefined){
             console.log('mute');
              scope.player.mute();
          }else if(!scope.mute && scope.player != undefined){
              console.log('unmute');
              scope.player.unMute();
          }
        }
      });


      youtubeApiLoader.ready.then(function() {
        scope.player = new YT.Player(element.children()[0], {
          playerVars: {
            autohide: scope.autoHide,
            autoplay: scope.autoPlay,
            cc_load_policy: scope.ccLoadPolicy,
            color: scope.color,
            controls: scope.control,
            disablekb: scope.disableKb,
            enablejsapi: scope.enableJsApi,
            end: scope.end,
            fs: scope.fs,
            hl: scope.hl,
            iv_load_policy: scope.ivLoadPolicy,
            list: scope.list,
            listType: scope.listType,
            loop: scope.loop,
            modestbranding: scope.modestBranding,
            origin: scope.origin,
            playerapiid: scope.playerApiId,
            playlist: scope.playlist,
            playsinline: scope.playsInline,
            rel: scope.rel,
            showinfo: scope.showInfo,
            start: scope.start,
            theme: scope.theme
          },

          height: scope.height,
          width: scope.width,
          videoId: scope.videoId,
          events: {
            'onReady': function(event) {
              console.log(scope.player);
              if(scope.mute){
                scope.player.mute();
              }
              scope.playerReady({
                event: event
              });
            },
            'onStateChange': function(event) {
              scope.stateChange({
                event: event
              });
            },
            'onPlaybackQualityChange': function(event) {
              scope.playbackQualityChange({
                event: event
              });
            },
            'onPlaybackRateChange': function(event) {
              scope.playbackRateChange({
                event: event
              });
            },
            'onError': function(event) {
              scope.error({
                event: event
              });
            },
            'onApiChange': function(event) {
              scope.apiChange({
                event: event
              });
            }
          }
        });
      });



    }
  };
}]);

angular.module('core').service('anchorSmoothScroll', [ function() {


    this.scrollTo = function(eID) {

        // This scrolling function 
        // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript
        
        var startY = currentYPosition();
        var stopY = elmYPosition(eID);
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY); return;
        }
        var speed = Math.round(distance / 100);
        if (speed >= 20) speed = 20;
        var step = Math.round(distance / 25);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for ( var i=startY; i<stopY; i+=step ) {
                setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
                leapY += step; if (leapY > stopY) leapY = stopY; timer++;
            } return;
        }
        for ( var i=startY; i>stopY; i-=step ) {
            setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
            leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
        }
        
        function currentYPosition() {
            // Firefox, Chrome, Opera, Safari
            if (self.pageYOffset) return self.pageYOffset;
            // Internet Explorer 6 - standards mode
            if (document.documentElement && document.documentElement.scrollTop)
                return document.documentElement.scrollTop;
            // Internet Explorer 6, 7 and 8
            if (document.body.scrollTop) return document.body.scrollTop;
            return 0;
        }
        
        function elmYPosition(eID) {
            var elm = document.getElementById(eID);
            var y = elm.offsetTop;
            var node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop;
            } return y;
        }

    };
    
}]);
'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('core').factory('Articles', ['$resource',
	function($resource) {
		return $resource('https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/articles/:articleId', {
			articleId: '@slug'
		}, {
			update: {
				method: 'PUT'
			},
			save: {
				url:'articles/',
				method: 'POST'
			}
		});
	}
]);

angular.module('core').service("stockService",["$http", '$filter','$rootScope',function($http, $filter, $rootScope){

	var getStock={};

		getStock.value = function(symbol,box,canvas){
      var simbolo = symbol;
      var valid = 0;
			symbolArray=[];
			priceArray=[];
			changeArray=[];
      chgpArray=[];
      numberArray=[];

      function reload(is){
              var url = 'http://query.yahooapis.com/v1/public/yql?q=select%20Name,Bid,Change,ChangeinPercent,symbol%20from%20yahoo.finance.quotes%20where%20symbol%20IN%20(';

              for(var i=0;i<simbolo.length;i++)
              {
                if(i == simbolo.length-1){
                  url = url+'%22'+simbolo[i]+'%22';
                  url = url+')&format=json&env=http://datatables.org/alltables.env&callback=JSON_CALLBACK';
                }else{
                  url = url+'%22'+simbolo[i]+'%22,';;
                }
              }

              $http.jsonp(url, {ignoreLoadingBar: true}).then(function(res) {
                 resp = res.data.query.results.quote;

                 if(is){
                   symbolArray=[];
                   priceArray=[];
                   changeArray=[];
                   chgpArray=[];
                   numberArray=[];
                 }

                 valid = 0;
                 for(i=0;i<resp.length;i++)
                 {
                   if(resp[i].Bid){
                     valid= valid+1;
                     symbol= resp[i].Name;
                     price= resp[i].Bid;
                     change= resp[i].Change;
                     chgp= resp[i].ChangeinPercent;
                     numberC = resp[i].Change;

                     if(is){
                       symbolArray.splice(i,0,symbol);
                       priceArray.splice(i,0,price);
                       changeArray.splice(i,0,change);
                       chgpArray.splice(i,0,chgp);
                       numberArray.splice(i,0,numberC);
                       swap(symbol,price,change,chgp);

                     }else{

                       symbolArray.push(symbol);
                       priceArray.push(price);
                       changeArray.push(change);
                       chgpArray.push(chgp);
                       numberArray.push(numberC);
                       swap(symbol,price,change,chgp);
                     }
                     if(i==resp.length-1 && is){
                       $rootScope.$broadcast('listo');

                     }
                    /* if(i==resp.length-1 && !is){
                       symbolArray.splice(0, (resp.length/2)-1);
                       priceArray.splice(0, (resp.length/2)-1);
                       changeArray.splice(0, (resp.length/2)-1);
                       chgpArray.splice(0, (resp.length/2)-1);
                       numberArray.splice(0, (resp.length/2)-1);
                       console.log(symbolArray);
                     }*/
                   }//Cierra IF no NULL

                 }
              });

      };

      reload(true);

			temp="";
			ext="";
      temp2="";
      ext2="";

      //VERSION 1
			box.fillStyle="#A83F23";
			box.fillRect(0,0,canvas.width,canvas.height);
			var posX=canvas.width;


			$rootScope.$on('$destroy', function() {
				 is = undefined;
				 clearInterval(interval1);
				 clearInterval(intervalload);
			 });

			interval1 = setInterval(moveText,30);
      var speed = 2;
      /*$rootScope.$on('listo',function(){
				if(valid > 0){
				var calc = 0;
				console.log(valid);
        calc = calc + (valid*5000);
				console.log(calc);
        intervalload = setInterval(reload,calc);
				}
      });*/

			function moveText(){
        // VERSION 1

				box.fillStyle="#A83F23";
				box.fillRect(0,0,canvas.width,canvas.height);
				posX-=speed;

					if(posX==-(ext.length)*20)
					{
						posX=canvas.width;
					}
				// instantiating text size to be 0
				sizeSymbol=0;sizePrice=0;sizeChange=0;sizeChangeP=0;sizetemp=0;
				box.fillStyle="black";
				box.beginPath();
				box.font="14px sans-serif";
				for(i=0;i<symbolArray.length;i++)
				{
					//calculates size of the text and adds .
					sizeSymbol=box.measureText(symbolArray[i]).width+sizetemp;
					sizePrice=sizeSymbol+box.measureText(priceArray[i]).width;
					sizeChange=sizePrice+box.measureText(changeArray[i]).width;
          sizeChangeP=sizeChange+box.measureText(chgpArray[i]).width;

					// assigning 3 fill text for 3 variables
						box.fillStyle="white"
						box.fillText(symbolArray[i]+" :",posX+sizetemp,20);
						box.fillStyle="white"
						box.fillText(priceArray[i],posX+sizeSymbol+20,20);
						if(numberArray[i]<0){
						box.fillStyle="red"}
						if(numberArray[i]>0){
            box.fillStyle="green"}
						box.fillText(changeArray[i]+" ("+chgpArray[i]+")",posX+sizePrice+30,20);

						// change the 150 value to increase + or decrease - distance in teh next cycle
						sizetemp=100+sizeChangeP;
				}

				box.closePath();

			}

			// swaping to for a string of all variables to get a string of all variables together
			function swap(symbol,price,changes,chgper)
			{
				temp=ext;
				ext=symbol+":"+price+","+change+"("+chgper+")";
				ext=temp+ext;

        temp2=ext;
        ext2=symbol+":"+price+","+change+"("+chgper+")";
        ext2=temp2+ext2;
			}

		};
		return getStock;

}]);

angular.module('core').factory('hashFactory', ["$http", "$rootScope", function($http, $rootScope) {

  return function(hash){
    var url = "https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/tags/"+hash;
    var obj =  $http.get(url,{ignoreLoadingBar: true, cache:true}).then(function(response){
            return response;
          });

    return obj;
  }

}]);

angular.module('core').factory('Reddit', ["$http", "Articles", "$rootScope", "$stateParams", "$location", function($http, Articles, $rootScope, $stateParams, $location) {
  var Reddit = function(cuenta, tipo, filtro, primero, page) {

    this.items = [];
    this.busy = false;
    this.stop = false;
    this.params = {};
    this.important = 0;
    var old = '';

    this.params.count = cuenta;
    this.params.filter = filtro;
    this.params.type = tipo;
    this.first = primero;
    this.params.page = page;

    if(this.first===undefined){ this.first= true;}
    if(this.params.count===undefined){this.params.count = 6;}
    if(this.first===true){this.params.page = undefined;}
    if(this.params.filter===undefined){this.params.filter = undefined;}
    if(this.params.type===undefined){this.params.type = undefined;}

  };

  Reddit.prototype.nextPage = function() {


    if (this.busy||this.stop){return};

    this.busy = true;
    var startUrl = "";


    if(this.params.type == 'tags' || this.params.type == 'category'){

        if(this.params.type == 'category'){
          this.params.filter = $rootScope.option[$stateParams.filtro].label;
        }



        var url = 'https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/tags/' +this.params.filter+"?count="+this.params.count;

    }else{

        var filterUrl = "";
        if(this.params.filter && this.params.type){
          filterUrl =  "&filter["+this.params.type+"]="+this.params.filter;
        }else{
          filterUrl = "";
        }

        var url = 'https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/articles?count='+this.params.count+filterUrl;

    }

    var config = {ignoreLoadingBar: !this.first};
    console.log(this.params.page);
    $http.post(url, {"lastKey":this.params.page} , config).then(function(response, stat, error){

        var data = response.data.results;
        if(response.data.message == 'No results' || response.data.results == undefined){
          this.stop = true;
          this.busy = false;
        }
        else if(data.length > 0){

            for (var i = 0; i < data.length; i++) {
                  this.items.push(data[i]);
            }
              $rootScope.$broadcast('cargador');
              if(data.length < this.params.count || !response.data.lastKey){
                this.stop = true;
              }else {
                this.params.page = JSON.stringify(response.data.lastKey);
              }
        }else{
          this.stop = true;
        }

      this.busy = false;

    }.bind(this));
  };
  return Reddit;
}]);

'use strict';

//Menu service used for managing  menus
angular.module('core').service('Menus', [

	function() {
		// Define a set of default roles
		this.defaultRoles = ['*'];

		// Define the menus object
		this.menus = {};

		// A private function for rendering decision 
		var shouldRender = function(user) {
			if (user) {
				if (!!~this.roles.indexOf('*')) {
					return true;
				} else {
					for (var userRoleIndex in user.roles) {
						for (var roleIndex in this.roles) {
							if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
								return true;
							}
						}
					}
				}
			} else {
				return this.isPublic;
			}

			return false;
		};

		// Validate menu existance
		this.validateMenuExistance = function(menuId) {
			if (menuId && menuId.length) {
				if (this.menus[menuId]) {
					return true;
				} else {
					throw new Error('Menu does not exists');
				}
			} else {
				throw new Error('MenuId was not provided');
			}

			return false;
		};

		// Get the menu object by menu id
		this.getMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			return this.menus[menuId];
		};

		// Add new menu object by menu id
		this.addMenu = function(menuId, isPublic, roles) {
			// Create the new menu
			this.menus[menuId] = {
				isPublic: isPublic || false,
				roles: roles || this.defaultRoles,
				items: [],
				shouldRender: shouldRender
			};

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			delete this.menus[menuId];
		};

		// Add menu item object
		this.addMenuItem = function(menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Push new menu item
			this.menus[menuId].items.push({
				title: menuItemTitle,
				link: menuItemURL,
				menuItemType: menuItemType || 'item',
				menuItemClass: menuItemType,
				uiRoute: menuItemUIRoute || ('/' + menuItemURL),
				isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].isPublic : isPublic),
				roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].roles : roles),
				position: position || 0,
				items: [],
				shouldRender: shouldRender
			});

			// Return the menu object
			return this.menus[menuId];
		};

		// Add submenu item object
		this.addSubMenuItem = function(menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === rootMenuItemURL) {
					// Push new submenu item
					this.menus[menuId].items[itemIndex].items.push({
						title: menuItemTitle,
						link: menuItemURL,
						uiRoute: menuItemUIRoute || ('/' + menuItemURL),
						isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].items[itemIndex].isPublic : isPublic),
						roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].items[itemIndex].roles : roles),
						position: position || 0,
						shouldRender: shouldRender
					});
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenuItem = function(menuId, menuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
					this.menus[menuId].items.splice(itemIndex, 1);
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeSubMenuItem = function(menuId, submenuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
					if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
						this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
					}
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		//Adding the topbar menu
		this.addMenu('topbar');
	}
]);
'use strict';

angular.module('core').factory('Vote', ['$resource',
	function($resource) {
		return $resource('https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/vote/:pollId', { pollId: '@_id'
		} ,
		{
 			save: {
 			method: 'POST',
 			params: { pollId: 'polls'},
 			ignoreLoadingBar: true
 		}
 	});
	}
]);

angular.module('core').factory('Stock', ["$http", "Articles", "$rootScope", "$interval", function($http, Articles, $rootScope, $interval) {

    var stock = Object;
    var interUpdate = undefined;

    var url = '';
    var simbolo = [];

    var start = function(symbols){

      simbolo = symbols || ['AAPL','GOOG','MELI','BFR','LND','BAK','BRFS','CBD','EDN','%5EDJI','%5EGSPC'];

      url = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quoteslist%20where%20symbol%20IN%20(';
      for(var i=0;i<simbolo.length;i++)
      {
        if(i == simbolo.length-1){
          url = url+'%22'+simbolo[i]+'%22';
          url = url+')&format=json&env=store://datatables.org/alltableswithkeys';
        }else{
          url = url+'%22'+simbolo[i]+'%22,';;
        }
      };
      update();
      interUpdate = $interval(update, 25000);
    };

    var update = function(){

      $http.get(url, {ignoreLoadingBar: true}).then( function(res){

        stock = res;
        $rootScope.$broadcast('skUpdate', res);
      });
    };

    var stop = function(){

       $interval.cancel(interUpdate);
       interUpdate = null;
       return
     };

     var stock = {
       start:start,
       update:update,
       stop:stop
     }

    return stock;


}]);

angular.module('core').factory('tweets', ["$http", "$rootScope", function($http, $rootScope) {

  var url = 'https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/twitter';
  var obj =  $http.get(url,{ignoreLoadingBar: true}).then(function(response){
          return response;
        });

  return obj;


}]);

angular.module('core').factory('Etiquetas', ["$http", "$location", "Tags", function($http, $location, Tags) {

  var Etiquetas = function() {
    var states = [];

    Tags.query({}, function(response) {

      var todo = response;

          angular.forEach(todo , function(element, i){
              states.push(element.label);
           });

      });

     return states;
  }

  return Etiquetas;



}]);

"use strict";

angular.module('core').factory('youtubeApiLoader', ['$q', '$window', function($q, $window) {
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  var loaded = false;
  var defer = $q.defer();

  $window.onYouTubeIframeAPIReady = function() {
    defer.resolve();
  };

  return {
    ready: defer.promise
  };
}]);
