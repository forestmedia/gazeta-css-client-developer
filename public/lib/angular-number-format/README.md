angular-number-format
=====================

소개 Introduction
-----------------

'input' 태그의 숫자에 쉼표(,)를 찍어주는 간단한 AngularJS 모듈입니다.<br>
A simple AngularJS directive that formats number in 'input' HTML tag.


사용 환경 Requirements
----------------------
 * Tested on AngularJS 1.2.4


예제 Examples
-------------
```html
Input: <input type="text" number-format ng-model="number">
Result: <span ng-bind="number"></span>
```

제한과 할일 Restrictions & Todos
--------------------------------
 * 현재는 양의 정수만 지원하고 있음. 음의 정수, 소수점도 지원하도록 구현해야 함.<br>Supports positive integer only. Float numbers to be supported.
