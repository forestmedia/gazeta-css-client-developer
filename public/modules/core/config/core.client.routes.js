'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider','cfpLoadingBarProvider',


	function($stateProvider, $urlRouterProvider, cfpLoadingBarProvider) {

		cfpLoadingBarProvider.includeBar = true;

		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html',
			controller:'HomeController'

		}).
		state('viewCategory', {
			url: '/noticias/:tipo/:filtro',
			templateUrl: 'modules/core/views/category.client.view.html',
			controller:'HomeController'

		}).
		state('viewUser', {
			url: '/:filtro',
			templateUrl: 'modules/core/views/category.client.view.html',
			controller:'ByUserController'
		}).
		state('viewTV', {
			url: '/noticias/:tipo',
			templateUrl: 'modules/core/views/tv.client.view.html',
			controller:'TvController'

		}).
		state('busqueda', {
			url: '/busqueda/:filtro',
			templateUrl: 'modules/core/views/busqueda.client.view.html',
			controller:'HomeController'
		}).
		state('busquedaTipo', {
			url: '/busqueda/:filtro/:tipo',
			templateUrl: 'modules/core/views/busqueda.client.view.html',
			controller:'HomeController'
		}).
		state('articleViewPublic', {
			url: '/articulo/:articuloId/:articuloDate',
			templateUrl: 'modules/core/views/article.client.view.html',
			controller:'HomeController'
		});

	}
]);

angular.module('core').run(['amMoment','$rootScope', '$http' ,function(amMoment, $rootScope, $http) {
    amMoment.changeLocale('es');

		$rootScope.modelo = {
			articles : {},
			banners : {},
			Política: {},
			Economía:{},
			Sucesos:{},
			Opinión:{},
			Imagen:{},
			update_date:"",
			widgets:{
		    tv :[],
		    poll:[],
		    custom:[],
		    likes:[],
		    popular:[],
		    hashtag:[],
		    user:[]
		  }
		}


		$rootScope.option = [
			  { label: 'Política', value: 0 },
			  { label: 'Economía', value: 1 },
			  { label: 'Sucesos', value: 2 },
			  { label: 'Opinión', value: 3 },
				{ label: 'Imagen', value: 4}
			];

		var Socket = {
		 socket: io.connect('http://gazeta-admin.us-east-1.elasticbeanstalk.com:8022')
	 	};

		$rootScope.isReady = false;

		Socket.socket.on('notification', function(response) {
				 $rootScope.modelo = response;
				 $rootScope.isReady = true;
				 console.log(response);
				 $rootScope.$broadcast('modelReady');
		});



}]);

angular.module('core').filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
