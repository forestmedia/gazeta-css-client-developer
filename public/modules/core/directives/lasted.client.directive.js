angular.module('core').component('lasted', {
  bindings: {
    articles: '<'
  },
  templateUrl: 'templates/lasted-client-template.html',
  controller: function ($scope, $interval,  $stateParams, $http, $rootScope, Stock) {

          $scope.news = [];

          if($stateParams.filtro==1)
          {
              $scope.stock=true;
          }else{
              $scope.stock=false;
          }

          $scope.conf = {
              news_length: false,
              news_pos: 200, // the starting position from the right in the news container
              news_margin: 20,
              news_move_flag: true
          };


          $scope.cambio = function(){
              $scope.stock=!$scope.stock;
          }

          var ticker =undefined;
          var reloader = undefined;


          $rootScope.$on('modelReady', function(){
                load();
          });

          $scope.$on('skUpdate', function(info, payload){
            $scope.data1 = payload.data.query.results.quote;
            if($scope.news){
              start();
            }
          });

          if($rootScope.isReady){
            load();
          }

          function load(){
            angular.fromJson($rootScope.modelo['articles'].results);
            $scope.news = angular.fromJson(angular.toJson($rootScope.modelo['articles'].results));
            // starting the interval by default
            if($scope.data1){
              start();
            }
          }


          $scope.calc = function(last, open){
            var change = open -last;
            return (change*100)/open;
          }

          function start(){
            stop();
            ticker = $interval($scope.news_move ,50);
          };

          // stops the interval
          function stop() {
              $interval.cancel(ticker);
              Stock.stop();
          };


          $scope.$on('$destroy', function() {
             stop();
           });


        $scope.get_news_right = function(idx) {
            var $right = $scope.conf.news_pos;
            for (var ri=0; ri < idx; ri++) {
                if (document.getElementById('news_'+ri)) {
                    $right += $scope.conf.news_margin + angular.element(document.getElementById('news_'+ri))[0].offsetWidth;
                }
            }
            return $right+'px';
        };

        $scope.news_move = function() {
            if (angular.element(document.getElementById('news_0'))[0]){
              if ($scope.conf.news_move_flag) {
                $scope.conf.news_pos--;
             if ( angular.element(document.getElementById('news_0'))[0].offsetLeft > angular.element(document.getElementById('news_strip'))[0].offsetWidth + $scope.conf.news_margin ) {
                   var first_new = $scope.news[0];
                    $scope.news.pop();
                    $scope.news.unshift(first_new);
                    $scope.conf.news_pos += angular.element(document.getElementById('news_0'))[0].offsetWidth + $scope.conf.news_margin;
                }
            }
          }
        };

  }
});
