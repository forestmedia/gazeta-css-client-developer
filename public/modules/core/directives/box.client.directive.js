angular.module('core').component('box', {
  bindings: {
    article: "<",
    tipo: "<"
  },
  templateUrl: 'templates/box-client-template.html',
  controller: 'box-controller',
  controllerAs: '$ctrl',
  bindToController: true
});
