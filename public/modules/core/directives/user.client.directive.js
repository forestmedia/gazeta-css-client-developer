angular.module('core').component('user',  {
	bindings: {
  	 username: "@"
  	},
    templateUrl: "templates/hash-client-template.html",
    controller: function($scope, $rootScope){
      $scope.hash = this.username;

      $rootScope.$on('modelReady', function(){
        load();
      });

			if($rootScope.isReady){
				load();
			}

			function load(){
				for (var i = 0; i < $rootScope.modelo.widgets.user.length; i++) {
          if($rootScope.modelo.widgets.user[i].title == $scope.hash){
            $scope.hashNews = $rootScope.modelo.widgets.user[i].results;
            break;
          }
        }
			}

    }

});
