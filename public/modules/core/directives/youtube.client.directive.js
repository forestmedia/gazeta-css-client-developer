angular.module('core').directive('youtube', ['$window', 'youtubeApiLoader', '$animate' ,function($window, youtubeApiLoader, $animate) {
  return {
    restrict: "E",

    scope: {
      stateChange: "&",
      playerReady: "&",
      playbackQualityChange: "&",
      playbackRateChange: "&",
      error: "&",
      apiChange: "&",
      videoId: "<", // required
      width: "@",
      height: "@",
      autoHide: "@",
      autoPlay: "@",
      ccLoadPolicy: "@",
      color: "@",
      control: "@",
      disableKb: "@",
      enableJsApi: "@",
      end: "@",
      fs: "@",
      hl: "@",
      ivLoadPolicy: "@",
      list: "@",
      listType: "@",
      loop: "@",
      modestBranding: "@",
      origin: "@",
      playerApiId: "@",
      playlist: "@",
      playsInline: "@",
      rel: "@",
      showInfo: "@",
      start: "@",
      theme: "@",
      mute: "="
    },

    template: '<div></div>',

    link: function(scope, element, attrs) {

    console.log(scope.videoId);

    $animate.addClass(element[0],'embed-responsive embed-responsive-16by9');
    $animate.addClass(element.children()[0],'embed-responsive-item');


      if(!scope.width && !scope.height){
        scope.width = 640;
        scope.height = 480;
      }

      scope.$watch('mute',function() {
        if(scope.mute != undefined){
          if(scope.mute && scope.player != undefined){
             console.log('mute');
              scope.player.mute();
          }else if(!scope.mute && scope.player != undefined){
              console.log('unmute');
              scope.player.unMute();
          }
        }
      });


      youtubeApiLoader.ready.then(function() {
        scope.player = new YT.Player(element.children()[0], {
          playerVars: {
            autohide: scope.autoHide,
            autoplay: scope.autoPlay,
            cc_load_policy: scope.ccLoadPolicy,
            color: scope.color,
            controls: scope.control,
            disablekb: scope.disableKb,
            enablejsapi: scope.enableJsApi,
            end: scope.end,
            fs: scope.fs,
            hl: scope.hl,
            iv_load_policy: scope.ivLoadPolicy,
            list: scope.list,
            listType: scope.listType,
            loop: scope.loop,
            modestbranding: scope.modestBranding,
            origin: scope.origin,
            playerapiid: scope.playerApiId,
            playlist: scope.playlist,
            playsinline: scope.playsInline,
            rel: scope.rel,
            showinfo: scope.showInfo,
            start: scope.start,
            theme: scope.theme
          },

          height: scope.height,
          width: scope.width,
          videoId: scope.videoId,
          events: {
            'onReady': function(event) {
              console.log(scope.player);
              if(scope.mute){
                scope.player.mute();
              }
              scope.playerReady({
                event: event
              });
            },
            'onStateChange': function(event) {
              scope.stateChange({
                event: event
              });
            },
            'onPlaybackQualityChange': function(event) {
              scope.playbackQualityChange({
                event: event
              });
            },
            'onPlaybackRateChange': function(event) {
              scope.playbackRateChange({
                event: event
              });
            },
            'onError': function(event) {
              scope.error({
                event: event
              });
            },
            'onApiChange': function(event) {
              scope.apiChange({
                event: event
              });
            }
          }
        });
      });



    }
  };
}]);
