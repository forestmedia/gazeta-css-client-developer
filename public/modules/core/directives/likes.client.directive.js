angular.module('core').component('likes',  {

    templateUrl: "templates/most-read-client-template.html",
    controller: function($scope, $rootScope){
  
      $rootScope.$on('modelReady', function(){
            load();
      });

      if($rootScope.isReady){
        load();
      }

      function load(){
        $scope.topNews = $rootScope.modelo.widgets.likes[0].results;
      }
    }

});
