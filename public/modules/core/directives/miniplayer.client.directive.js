'use strict';
angular.module('core').component('miniplayer', {
  bindings:{
    title:'@',
    mini:'<',
    look:'@'
  },
  controllerAs: '$ctrl',
  bindToController: true,
  templateUrl: "templates/miniplayer-client-template.html",
  controller:function($animate, $element, $scope, $location){

    this.big = false;
    this.mute = true;

    this.vol = function(){
      this.mute = !this.mute;
    }

    this.totv = function(){
      $location.path('noticias/tv');
    }

    this.bigg = function(){
      this.big = true;
      $animate.addClass($element.children()[0],'bigg');

    };

   this.smal =  function(){
      this.big = false;
      $animate.removeClass($element.children()[0],'bigg');

    };

    this.look = false;

    this.close = function(){
      this.look = !this.look;
    }
  }
});
