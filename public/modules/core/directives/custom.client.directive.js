angular.module('core').component('custom',  {
	bindings: {
  	 title: "@"
  	},
    templateUrl: "templates/hash-client-template.html",
    controller: function($scope, $rootScope){

      $scope.hash = this.title;


      $rootScope.$on('modelReady', function(){
            load();
      });

      if($rootScope.isReady){
        load();
      }

      function load(){
				for (var i = 0; i < $rootScope.modelo.widgets.custom.length; i++) {
					if($rootScope.modelo.widgets.custom[i].title == $scope.hash){
						$scope.hashNews = $rootScope.modelo.widgets.custom[i];
						break;
					}
				}
      }


    }

});
