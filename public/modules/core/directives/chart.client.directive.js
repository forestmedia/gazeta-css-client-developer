angular.module('core').component('stock',{

  	bindings: {
  		symbols: "<",
  		tipo: "@",
      title:"@"
  	},
    templateUrl: "templates/chart-client-template.html",
    controller: function ($scope, Stock) {


      Stock.start(this.symbols);

      $scope.$on('skUpdate', function(info, payload){
      	$scope.data1 = payload.data.query.results.quote;
      });
      $scope.$on('$destroy', function(info, payload){
      	Stock.stop();
      });

      $scope.calc = function(last, open){
      	var change = open -last;
      	return (change*100)/open;
      }
    }
});
