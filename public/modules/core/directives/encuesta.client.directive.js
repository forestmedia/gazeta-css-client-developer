angular.module('core').component('poll',  {

    bindings:{
      id:"@"
    },
    templateUrl: "templates/encuesta-client-template.html",
    controller:function($scope, $http, $location , Vote, $rootScope) {

    var idn = this.id;

    $scope.seleccion = {indice:0};


      $rootScope.$on('modelReady', function(){
          load();
      });

      if($rootScope.isReady){
        load();
      }

      function load(){

  				for (var i = 0; i < $rootScope.modelo.widgets.poll.length; i++) {
  						if($rootScope.modelo.widgets.poll[i].title == idn){
  							$scope.quest = $rootScope.modelo.widgets.poll[i].results;
  							break;
  						}
  				}

          if($scope.quest){
    				$http.get('https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/ip?_id='+$scope.quest._id, {cache: true})
    					.then(function(res) {

    					if(res.data.message == 'exist'){
    						$scope.resultView = true;
    						$scope.voted = true;
    					}else if(res.data.message == 'available'){
    						$scope.resultView = false;
    						$scope.voted = false;
    					}
    				});
          }
      }

  		$scope.vote = function() {
  			var vote = new Vote ({
  				pollId: $scope.quest._id,  textPos:$scope.seleccion.indice
  			});

  			vote.$save({
  				pollId: $scope.quest._id,  textPos:$scope.seleccion.indice
  			},function(error, res) {
            if(res.data.message === 'success'){
              $scope.quest.options[$scope.seleccion.indice].vote++;
              $scope.resultView = true;
              $scope.voted = true;
            }else{
              console.error('culo');
            }
  			});
  		};

  		$scope.resultBtn = function() {
  			$scope.resultView = !$scope.resultView;
  		};

  }
});
