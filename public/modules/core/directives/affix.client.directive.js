'use strict';
angular.module('core').component('affixer', {

  controller: function ($scope, $element, $document, $window, $animate) {

      var win = angular.element($window);
      var agregado = false;
      affixElement();

      function showClass(){

        $animate.addClass($element[0],'fticker');
        agregado = true;
         //$scope.$digest();
      };

      function hideClass(){

        $animate.removeClass($element[0],'fticker');
        agregado = false;
         //$scope.$digest();
      };

      function affixElement() {

          if ($window.pageYOffset > 800 && !agregado) {
              showClass();
          } else if ($window.pageYOffset < 800 && agregado) {
              hideClass()
          }
      };

      $scope.$on('$routeChangeStart', function() {
          win.unbind('scroll', affixElement);
      });
      win.bind('scroll', affixElement);
  }
})

angular.module('core').directive('stickyfill', [function($scope) {
  return {
    restrict: 'AEC',
    scope: {
        top: "<",
      },
    link: function(scope, element, window) {

      var tope = scope.top || 50;

      element.css({
        top: tope+'px'
      });
      Stickyfill.add(element[0]);
    }
  };
}]);
