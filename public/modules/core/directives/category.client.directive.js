angular.module('core').component('category',  {

	bindings: {
  		idcat: "<",
  		tipo: "<"

  	},
    templateUrl: "templates/category-client-template.html",
    controller:function($rootScope, $scope){

      var categoria = $rootScope.option[this.idcat].label;


      //Tipo (1. Cascada 2. Sidebar)
      if(!this.tipo)
      {$scope.tipo=1;}

      $rootScope.$on('modelReady', function(){
          load();
      });

      if($rootScope.isReady){
        load();
      }

      function load(){
        $scope.catNews = $rootScope.modelo[categoria].results;
      }

    }

});
