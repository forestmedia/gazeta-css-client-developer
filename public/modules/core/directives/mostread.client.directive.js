angular.module('core').component('popular',  {

    templateUrl: "templates/most-read-client-template.html",
    controller: function($scope, $rootScope){
      $rootScope.$on('modelReady', function(){
            load();
      });

      if($rootScope.isReady){
        load();
      }

      function load(){
        $scope.topNews = $rootScope.modelo.widgets.popular[0].results;
      }
    }

});
