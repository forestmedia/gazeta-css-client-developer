angular.module('core').component('tv',  {
	bindings: {
  	 title: "@",
     tipo: "@"
  	},
    templateUrl: "templates/videos-client-template.html",
    controller: function($scope, $uibModal, $rootScope, $document) {

    var title = this.title;
    $scope.tipo = this.tipo;

    $rootScope.$on('modelReady', function(){
			load();
    });

		if($rootScope.isReady){
			load();
		}

		function load(){
			for (var i = 0; i < $rootScope.modelo.widgets.tv.length; i++) {
				if($rootScope.modelo.widgets.tv[i].title == title){
					$scope.youNews = $rootScope.modelo.widgets.tv[i].results;
					break;
				}
			}
		}

    $scope.clickVideo = function(id , title){
    	$rootScope.playVideo = id;
    	$rootScope.videoTitle = title;

    	var modalInstance = $uibModal.open({
    		// Put a link to your template here or whatever
    		templateUrl: 'modules/core/views/player.html',
				windowClass: 'tv'

    	});
    }

    }

});
