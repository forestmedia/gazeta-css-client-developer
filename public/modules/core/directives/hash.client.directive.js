angular.module('core').component('hashtag',  {
	bindings: {
  	 hash: "@"
  	},
    templateUrl: "templates/hash-client-template.html",
    controller: function($scope, $rootScope){
      $scope.hash = this.hash;

			$rootScope.$on('modelReady', function(){
						load();
			});

			if($rootScope.isReady){
				load();
			}

			function load(){
				for (var i = 0; i <$rootScope.modelo.widgets.hashtag.length; i++) {
						 if($rootScope.modelo.widgets.hashtag[i].title == $scope.hash){
							 $scope.hashNews = $rootScope.modelo.widgets.hashtag[i].results;
							 break;
						 }
				 }
			}
			
    }

});
