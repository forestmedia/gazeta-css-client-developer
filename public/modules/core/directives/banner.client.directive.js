'use strict';
angular.module('core').component('banner', {

  bindings: {
    banners: "<"
  },

  templateUrl: "templates/banner-client-template.html",
  controller: function ($scope) {

      var ram = Number;

    this.$onChanges = function (changesObj) {
      if (changesObj.banners) {
        if(changesObj.banners.currentValue != undefined){
          this.banners = changesObj.banners.currentValue;
          ram = (Math.floor(this.banners.length * Math.random() + 1))-1;
          $scope.banner = this.banners[ram];
        }
      }
    };


  }
});
