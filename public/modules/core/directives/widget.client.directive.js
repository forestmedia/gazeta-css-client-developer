'use strict';
angular.module('core').component('widget', {
  bindings: {
    section: '@',
    pos: '@'
  },
  bindToController: true,
  controllerAs: '$ctrl',
  templateUrl:"templates/widgets-client-template.html",
  controller: function ($scope, $element, $compile, $http) {
    var where = this.section;
    var url = "http://gazeta-ccs-files.s3-website-us-west-2.amazonaws.com/config.json";
    $http.get(url).then(function(response, err){
      angular.forEach(response.data, function(element){

          if(element.label == where){
            $scope.widgets = element.widget;
          }
      });
    });

    var pos = this.pos;
    $scope.compilar = function(element){
      var add =""
      if(element.type == 'category'){
        add = "idcat='"+element.category.value+"'";
      }
      if(element.type == 'hashtag'){
        add = "hash='"+element.input+"'";

      }

      if(element.type == 'tv'){
        add = "title='"+element.input+"' tipo='"+pos+"'";
      }
      
      if(element.type == 'user'){
        add = "username='"+element.input+"'";

      }
      if(element.type == 'custom'){
        add = "title='"+element.input+"'";
      }
      if(element.type == 'poll'){
        add = "id='"+element.input+"'";
      }
      if(element.type == 'stock'){
        var arr = '';
        angular.forEach(element.arrayElement, function(elm, key){
          arr = arr + '"' + elm +'"';
          if(element.arrayElement.length - 1 > key){
            arr = arr + ',';
          }
        });
        add = "symbols='["+arr+"]' tipo='"+pos+"' title='"+element.input+"'";
      }
      if(element.type == 'banner'){
        if(element.posicion == 'Lateral'){
          add = "banners='banners_B1'";
        }else{
          add = "banners='banners_P1'";
        }
      }

      var wid = angular.element('<'+element.type+' '+add+'></'+element.type+'>');
      $element.append(wid);
      $compile(wid)($scope);
    }


  }
});
