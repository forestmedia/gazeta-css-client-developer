angular.module('core').component('imageday',  {
    templateUrl: "templates/image-day-client-template.html",
    controller: function($rootScope, $scope){
                $rootScope.$on('modelReady', function(){
                      load();
                });

                if($rootScope.isReady){
                  load();
                }

                function load(){
                  $scope.imageNews = $rootScope.modelo.Imagen.results[0];
                }
    }

});
