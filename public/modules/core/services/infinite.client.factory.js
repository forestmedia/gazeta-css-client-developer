angular.module('core').factory('Reddit', function($http, Articles, $rootScope, $stateParams, $location) {
  var Reddit = function(cuenta, tipo, filtro, primero, page) {

    this.items = [];
    this.busy = false;
    this.stop = false;
    this.params = {};
    this.important = 0;
    var old = '';

    this.params.count = cuenta;
    this.params.filter = filtro;
    this.params.type = tipo;
    this.first = primero;
    this.params.page = page;

    if(this.first===undefined){ this.first= true;}
    if(this.params.count===undefined){this.params.count = 6;}
    if(this.first===true){this.params.page = undefined;}
    if(this.params.filter===undefined){this.params.filter = undefined;}
    if(this.params.type===undefined){this.params.type = undefined;}

  };

  Reddit.prototype.nextPage = function() {


    if (this.busy||this.stop){return};

    this.busy = true;
    var startUrl = "";


    if(this.params.type == 'tags' || this.params.type == 'category'){

        if(this.params.type == 'category'){
          this.params.filter = $rootScope.option[$stateParams.filtro].label;
        }



        var url = 'https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/tags/' +this.params.filter+"?count="+this.params.count;

    }else{

        var filterUrl = "";
        if(this.params.filter && this.params.type){
          filterUrl =  "&filter["+this.params.type+"]="+this.params.filter;
        }else{
          filterUrl = "";
        }

        var url = 'https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/articles?count='+this.params.count+filterUrl;

    }

    var config = {ignoreLoadingBar: !this.first};
    console.log(this.params.page);
    $http.post(url, {"lastKey":this.params.page} , config).then(function(response, stat, error){

        var data = response.data.results;
        if(response.data.message == 'No results' || response.data.results == undefined){
          this.stop = true;
          this.busy = false;
        }
        else if(data.length > 0){

            for (var i = 0; i < data.length; i++) {
                  this.items.push(data[i]);
            }
              $rootScope.$broadcast('cargador');
              if(data.length < this.params.count || !response.data.lastKey){
                this.stop = true;
              }else {
                this.params.page = JSON.stringify(response.data.lastKey);
              }
        }else{
          this.stop = true;
        }

      this.busy = false;

    }.bind(this));
  };
  return Reddit;
});
