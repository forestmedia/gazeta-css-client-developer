angular.module('core').factory('Stock', function($http, Articles, $rootScope, $interval) {

    var stock = Object;
    var interUpdate = undefined;

    var url = '';
    var simbolo = [];

    var start = function(symbols){

      simbolo = symbols || ['AAPL','GOOG','MELI','BFR','LND','BAK','BRFS','CBD','EDN','%5EDJI','%5EGSPC'];

      url = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quoteslist%20where%20symbol%20IN%20(';
      for(var i=0;i<simbolo.length;i++)
      {
        if(i == simbolo.length-1){
          url = url+'%22'+simbolo[i]+'%22';
          url = url+')&format=json&env=store://datatables.org/alltableswithkeys';
        }else{
          url = url+'%22'+simbolo[i]+'%22,';;
        }
      };
      update();
      interUpdate = $interval(update, 25000);
    };

    var update = function(){

      $http.get(url, {ignoreLoadingBar: true}).then( function(res){

        stock = res;
        $rootScope.$broadcast('skUpdate', res);
      });
    };

    var stop = function(){

       $interval.cancel(interUpdate);
       interUpdate = null;
       return
     };

     var stock = {
       start:start,
       update:update,
       stop:stop
     }

    return stock;


});
