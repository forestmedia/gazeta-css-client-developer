'use strict';

angular.module('core').factory('Vote', ['$resource',
	function($resource) {
		return $resource('https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/vote/:pollId', { pollId: '@_id'
		} ,
		{
 			save: {
 			method: 'POST',
 			params: { pollId: 'polls'},
 			ignoreLoadingBar: true
 		}
 	});
	}
]);
