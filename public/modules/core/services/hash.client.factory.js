angular.module('core').factory('hashFactory', function($http, $rootScope) {

  return function(hash){
    var url = "https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/tags/"+hash;
    var obj =  $http.get(url,{ignoreLoadingBar: true, cache:true}).then(function(response){
            return response;
          });

    return obj;
  }

});
