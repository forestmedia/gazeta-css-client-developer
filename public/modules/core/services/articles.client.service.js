'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('core').factory('Articles', ['$resource',
	function($resource) {
		return $resource('https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/articles/:articleId', {
			articleId: '@slug'
		}, {
			update: {
				method: 'PUT'
			},
			save: {
				url:'articles/',
				method: 'POST'
			}
		});
	}
]);
