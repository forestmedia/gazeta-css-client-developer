angular.module('core').service("stockService",["$http", '$filter','$rootScope',function($http, $filter, $rootScope){

	var getStock={};

		getStock.value = function(symbol,box,canvas){
      var simbolo = symbol;
      var valid = 0;
			symbolArray=[];
			priceArray=[];
			changeArray=[];
      chgpArray=[];
      numberArray=[];

      function reload(is){
              var url = 'http://query.yahooapis.com/v1/public/yql?q=select%20Name,Bid,Change,ChangeinPercent,symbol%20from%20yahoo.finance.quotes%20where%20symbol%20IN%20(';

              for(var i=0;i<simbolo.length;i++)
              {
                if(i == simbolo.length-1){
                  url = url+'%22'+simbolo[i]+'%22';
                  url = url+')&format=json&env=http://datatables.org/alltables.env&callback=JSON_CALLBACK';
                }else{
                  url = url+'%22'+simbolo[i]+'%22,';;
                }
              }

              $http.jsonp(url, {ignoreLoadingBar: true}).then(function(res) {
                 resp = res.data.query.results.quote;

                 if(is){
                   symbolArray=[];
                   priceArray=[];
                   changeArray=[];
                   chgpArray=[];
                   numberArray=[];
                 }

                 valid = 0;
                 for(i=0;i<resp.length;i++)
                 {
                   if(resp[i].Bid){
                     valid= valid+1;
                     symbol= resp[i].Name;
                     price= resp[i].Bid;
                     change= resp[i].Change;
                     chgp= resp[i].ChangeinPercent;
                     numberC = resp[i].Change;

                     if(is){
                       symbolArray.splice(i,0,symbol);
                       priceArray.splice(i,0,price);
                       changeArray.splice(i,0,change);
                       chgpArray.splice(i,0,chgp);
                       numberArray.splice(i,0,numberC);
                       swap(symbol,price,change,chgp);

                     }else{

                       symbolArray.push(symbol);
                       priceArray.push(price);
                       changeArray.push(change);
                       chgpArray.push(chgp);
                       numberArray.push(numberC);
                       swap(symbol,price,change,chgp);
                     }
                     if(i==resp.length-1 && is){
                       $rootScope.$broadcast('listo');

                     }
                    /* if(i==resp.length-1 && !is){
                       symbolArray.splice(0, (resp.length/2)-1);
                       priceArray.splice(0, (resp.length/2)-1);
                       changeArray.splice(0, (resp.length/2)-1);
                       chgpArray.splice(0, (resp.length/2)-1);
                       numberArray.splice(0, (resp.length/2)-1);
                       console.log(symbolArray);
                     }*/
                   }//Cierra IF no NULL

                 }
              });

      };

      reload(true);

			temp="";
			ext="";
      temp2="";
      ext2="";

      //VERSION 1
			box.fillStyle="#A83F23";
			box.fillRect(0,0,canvas.width,canvas.height);
			var posX=canvas.width;


			$rootScope.$on('$destroy', function() {
				 is = undefined;
				 clearInterval(interval1);
				 clearInterval(intervalload);
			 });

			interval1 = setInterval(moveText,30);
      var speed = 2;
      /*$rootScope.$on('listo',function(){
				if(valid > 0){
				var calc = 0;
				console.log(valid);
        calc = calc + (valid*5000);
				console.log(calc);
        intervalload = setInterval(reload,calc);
				}
      });*/

			function moveText(){
        // VERSION 1

				box.fillStyle="#A83F23";
				box.fillRect(0,0,canvas.width,canvas.height);
				posX-=speed;

					if(posX==-(ext.length)*20)
					{
						posX=canvas.width;
					}
				// instantiating text size to be 0
				sizeSymbol=0;sizePrice=0;sizeChange=0;sizeChangeP=0;sizetemp=0;
				box.fillStyle="black";
				box.beginPath();
				box.font="14px sans-serif";
				for(i=0;i<symbolArray.length;i++)
				{
					//calculates size of the text and adds .
					sizeSymbol=box.measureText(symbolArray[i]).width+sizetemp;
					sizePrice=sizeSymbol+box.measureText(priceArray[i]).width;
					sizeChange=sizePrice+box.measureText(changeArray[i]).width;
          sizeChangeP=sizeChange+box.measureText(chgpArray[i]).width;

					// assigning 3 fill text for 3 variables
						box.fillStyle="white"
						box.fillText(symbolArray[i]+" :",posX+sizetemp,20);
						box.fillStyle="white"
						box.fillText(priceArray[i],posX+sizeSymbol+20,20);
						if(numberArray[i]<0){
						box.fillStyle="red"}
						if(numberArray[i]>0){
            box.fillStyle="green"}
						box.fillText(changeArray[i]+" ("+chgpArray[i]+")",posX+sizePrice+30,20);

						// change the 150 value to increase + or decrease - distance in teh next cycle
						sizetemp=100+sizeChangeP;
				}

				box.closePath();

			}

			// swaping to for a string of all variables to get a string of all variables together
			function swap(symbol,price,changes,chgper)
			{
				temp=ext;
				ext=symbol+":"+price+","+change+"("+chgper+")";
				ext=temp+ext;

        temp2=ext;
        ext2=symbol+":"+price+","+change+"("+chgper+")";
        ext2=temp2+ext2;
			}

		};
		return getStock;

}]);
