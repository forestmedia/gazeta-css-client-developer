angular.module('core').factory('tweets', function($http, $rootScope) {

  var url = 'https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/twitter';
  var obj =  $http.get(url,{ignoreLoadingBar: true}).then(function(response){
          return response;
        });

  return obj;


});
