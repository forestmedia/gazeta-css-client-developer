'use strict';

angular.module('core').controller('HomeController', ['$scope', '$stateParams', '$location','Articles', 'Reddit','cfpLoadingBar','$rootScope', 'ga', '$http','$document','$window',
	function($scope, $stateParams, $location, Articles , Reddit, cfpLoadingBar, $rootScope, ga, $http,  $document, $window) {

		$scope.etiqueta = {label:"estudiantes"};
		$scope.link = 'chart.finance.yahoo.com/z?s=GOOG&t=1d&q=c&l=on&z=l&p=m5,m200';
		$scope.Player = 'njCDZWTI-xg';

		//$scope.link = 'https://www.google.com/finance/getchart?q=GOOG';


// GOOGLE ANALITICS CARGADOR
  ga('create', 'UA-60745039-1', 'www.gazetadecaracas.com');
  ga('send', 'pageview');

// ASYNC DETECTOR DE LAS CARGAS
  $rootScope.$on ("cfpLoadingBar:loading",function (event, data) {
   	$scope.cargado = false;
      return
	});
  $rootScope.$on ("cfpLoadingBar:completed",function (event, data) {
		 $window.prerenderReady = true;
   	$scope.cargado = true;
      return
	});


$rootScope.$on("$viewContentLoaded",
function() {
	$rootScope.cargado = true;
	$window.scrollTo(0,0);
	if($rootScope.isReady){
		load();
	}
});


	$rootScope.$on('modelReady', function(){
		load();
	});

	function load(){
		$rootScope.banners_P1 = [];
		$rootScope.banners_A1 = [];
		$rootScope.banners_B1 = [];
		$rootScope.banners_B2 = [];
		$rootScope.banners_B3 = [];
		angular.forEach($rootScope.modelo.banners , function(element, i){
			if(element.format.value == 2){
					$rootScope.banners_P1.push(element);
				}
			if(element.format.value == 0){
					$rootScope.banners_A1.push(element);
				}
			if(element.format.value == 1){
					$rootScope.banners_B1.push(element);
				}
			if(element.format.value == 3){
					$rootScope.banners_B2.push(element);
				}
			if(element.format.value == 4){
					$rootScope.banners_B3.push(element);
				}
		});

		if($stateParams.tipo == 'category'){
			$scope.articles = $rootScope.modelo[$rootScope.option[$stateParams.filtro].label];
			var lastKey = JSON.stringify($rootScope.modelo[$rootScope.option[$stateParams.filtro].label].lastKey);
			var filter = $stateParams.filtro;
			$scope.idcat = $stateParams.filtro;
			var type = $stateParams.tipo ;
			$scope.contenedor = new Reddit(6,type,filter,false,lastKey);
		}else{
			$scope.articles = $rootScope.modelo.articles;
			var lastKey = JSON.stringify($rootScope.modelo.articles.lastKey);
			$scope.reddit = new Reddit(6,undefined,undefined,false, lastKey);
		}
	}

		// PETICION PARA ARTICULO INDIVIDUAL
		$scope.findOne = function() {
		$scope.readyDisq = false;
		Articles.get({
							articleId: $stateParams.articuloId,
							articleDate: $stateParams.articuloDate
						}, function(response){
							var disqID = 'gazetaccs';
							var url = "https://www.gazetadecaracas.com/#!/articulo/"+response.slug+"/"+response.created_date;
							$scope.article = response;
							$scope.disqusConfig = {
									disqus_shortname: disqID,
									disqus_identifier: response._id,
									disqus_title: response.title,
									disqus_url: url
							};
						});
		};

}

]);
