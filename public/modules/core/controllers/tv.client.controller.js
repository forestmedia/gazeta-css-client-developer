angular.module('core').controller('TvController', ['$scope', '$http', '$location', '$uibModal','$rootScope', '$timeout', '$window',
	function($scope, $http, $location, $uibModal, $rootScope, $timeout, $window) {

	var activo = true;
	var origin = "https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/videos/"
	var playlist1 = origin+"PLFh862iGfy0kkjcTnVDRs1mw__94WARq5";
	var playlist2 = origin+"PLlTLHnxSVuIz6HF8xMupeHpOtTqbyDLW0";
	var playlist3 = origin+"PLlTLHnxSVuIyn2-PlKO6P0FkWZqb0uczg";
	var playlist4 = origin+"PLlTLHnxSVuIyw5jPrLmewrpBJAPYKhgml";
	var playlist5 = origin+"PLlTLHnxSVuIzxAaEd8QDHerZDyJ-AK2Yr";
	var playlist6 = origin+"PLlTLHnxSVuIy0oTb_bFBJL6rfUazkb3y7";
	var playlist7 = origin+"PLlTLHnxSVuIxpJYB7uxXS9PZhHNwTEzOA";
	var playlist8 = origin+"PLlTLHnxSVuIyn2-PlKO6P0FkWZqb0uczg";

	$scope.Player = 'njCDZWTI-xg';
	$scope.auto = 1;

	angular.element(document.getElementsByClassName( 'contenedor' )).bind('scroll', function(){
		$rootScope.$broadcast('tv');
	});

	$scope.initVideos = function(){

		$http.get(playlist1,{ignoreLoadingBar: true, cache:true}).then(
			function(data){
				$rootScope.videosTV1 = data;
			});

		$http.get(playlist2,{ignoreLoadingBar: true, cache:true}).then(function(data){ console.log(data);$scope.videosTV2 = data});
		$http.get(playlist3,{ignoreLoadingBar: true, cache:true}).then(function(data){$scope.videosTV3 = data});
		$http.get(playlist4,{ignoreLoadingBar: true, cache:true}).then(function(data){$scope.videosTV4 = data});
		$http.get(playlist5,{ignoreLoadingBar: true, cache:true}).then(function(data){$scope.videosTV5 = data});
		$http.get(playlist6,{ignoreLoadingBar: true, cache:true}).then(function(data){$scope.videosTV6 = data});
		$http.get(playlist7,{ignoreLoadingBar: true, cache:true}).then(function(data){$scope.videosTV7 = data});
		$http.get(playlist8,{ignoreLoadingBar: true, cache:true}).then(function(data){$scope.videosTV8 = data});

	}


	$scope.clickVideo = function(id , title){

		$rootScope.playVideo = id;
		$rootScope.videoTitle = title;

		var modalInstance = $uibModal.open({
			// Put a link to your template here or whatever
			templateUrl: 'modules/core/views/player.html',
			windowClass: 'tv',
		});
	}

	$scope.clickPic = function(){
      		$scope.activo = !$scope.activo;
	}


}]);
