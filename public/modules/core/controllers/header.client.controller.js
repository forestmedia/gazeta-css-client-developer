'use strict';

angular.module('core').controller('HeaderController', ['$scope',  'Menus', '$http',"$location", '$aside','$rootScope','$document','$timeout',
	function($scope,  Menus, $http, $location, $aside, $rootScope, $document,$timeout) {

		$scope.isCollapsed = false;
		$scope.searchView = false;

//aside
		$scope.asideState = {
		      open: false
		};

    $scope.openAside = function(position, backdrop) {
      $scope.asideState = {
        open: true,
        position: position
      };

      function postClose() {
        $scope.asideState.open = false;
      }

      $aside.open({
        templateUrl: 'modules/core/views/aside.html',
        placement: position,
        size: 'xs',
				windowClass: 'menumo',
        backdrop: backdrop,
        controller: function($scope, $uibModalInstance) {
          $scope.ok = function(e) {
            $uibModalInstance.close();
            e.stopPropagation();
          };
          $scope.cancel = function(e) {
            $uibModalInstance.dismiss();
            e.stopPropagation();
          };
        }
      }).result.then(postClose, postClose);
		}
//END aside



		$scope.typeaheadOpts = {
		  minLength: 3,
		  waitMs: 500,
		  allowsEditable: true
		};

		$scope.menu = Menus.getMenu('topbar');

		var clickIn = false;

		$document.bind('click', function() {

			$timeout(function () {
				if(!clickIn){

					$scope.searchView = false;
					clickIn = false;
				}else{

					clickIn = false;
				}
			}, 5);

		});

		$scope.inside = function() {
			clickIn = true;
		};


		$scope.onClick = function(val){

			$scope.searchView = false;

			if(val.id){

				$location.path("/articulo/"+val.id);
				$scope.searchView = !$scope.searchView;
			}
			else {

				$location.path("/busqueda/"+val);
				$scope.searchView = !$scope.searchView;

			}
		}

		$scope.changeSearch = function() {
			$scope.searchView = !$scope.searchView;
		};

		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});

		$scope.tags = ["SIMADI","ISIS","Gazeta", "Test", "Noticia"];

		$scope.queryArray = function(val) {

			var tipos = ["title","headlines"];
			var todo = [];
			var print = {}

			angular.forEach(tipos, function(tipo,key){

					todo.push($http.get('articles?count=6&filter['+ tipo +']='+val, {ignoreLoadingBar: true
									}).then(function(response){
										return response.data.results.map(function(items){
												return items;
									 });
								}));
			});

			print = todo[0].then(function(res){ return res});
			return print&&todo[1];


	};






	}]);
