angular.module('core').controller('ByUserController', ['$scope','Reddit', '$http', '$stateParams', '$window','$location',
	function($scope, Reddit, $http, $stateParams, $window, $location) {

		$scope.tagview = false;

		$http.get("user/"+$stateParams.filtro, {ignoreLoadingBar:true}).then( function(res){
			if(res.data == null){ $scope.tagview = true;}else{$scope.profile = res.data;}
		});


		var filter = $stateParams.filtro;
		$scope.idcat = $stateParams.filtro;
		var type = 'tags' ;
		$scope.contenedor = new Reddit(6,type,filter,false);

		// ASYNC DETECTOR DE LAS CARGAS
    $scope.$on ("cfpLoadingBar:loading",function (event, data) {
     	$scope.cargado = false;
        return
		});
    $scope.$on ("cfpLoadingBar:completed",function (event, data) {
     	$scope.cargado = true;
        return
		});


		$scope.$on("$viewContentLoaded",
		function() {
			$scope.cargado = true;
			$window.scrollTo(0,0);
		});

}]);
