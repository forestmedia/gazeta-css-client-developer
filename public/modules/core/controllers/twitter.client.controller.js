angular.module('core').controller('TwitterController', ['$scope', 'tweets',
	function($scope, tweets) {

			tweets.then(function(res){
				$scope.tweets = res;
			}
		);

		$scope.formatDate = function(fecha){
				var modificado  = new Date(fecha.replace(/-/g,"/"));
				return modificado;
		}

}]);
